all: doc keys

keys:
	@cp ~/.ssh/authorized_keys static/authorized_keys
	@gpg --export --no-version --armor --fingerprint \
		CDB0C0F904F4EE9B \
		> static/CDB0C0F904F4EE9B.asc
	@gpg --export --no-version --armor --fingerprint \
		CDB0C0F904F4EE9B \
		56E5D851BED31E42 \
		D54F3FBC4E55FB37 \
		7B6D7CF1E659C86A \
		> static/KEYS.asc

doc:
	@rm -rf public
	@hugo -v
	@chmod -R +r public
	@find public -type d | xargs chmod +xr

publish: keys doc
	@rclone sync public/ asf:public_html/ \
		--config=rclone.conf \
		--exclude=snapshots \
		--exclude=dist \
		--verbose

.PHONY	: doc publish

