---
title: "Inside Windows Debugging"
date: 2012-09-14
comments: false
categories: [windows, reviews]
---

This book is awesome. It will become your bible for windows debugging,
and covers both theoretical aspects such as how the kernel & userland
fit together in Windows, details on how 64 bit & 32 bit cohabit etc, and
real-world expertise on debugging strategies for remote, local, and
virtual machine scenarios. If you are programming or supporting Windows
apps & servers, then you want this book. For novices, if you've not
debugged an NT BSOD yet then this will give you enough understanding and
straightforward tips to do so yourself, and for experts, if you don't
learn something new from this book I'd be very surprised. The kindle
edition is easy to read most of the time (on my large-screen DX), and at
500+ pages there's a lot of great material to work through.
I'll say it again: Awesome.

This review was brought to you by the O'Reilly [blogger programme].

[blogger programme]: http://oreilly.com/bloggers/?cmp=ex-orm-blgr-dave-cottlehuber
