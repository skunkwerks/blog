---
title: "setup rails on windows to use SQLite3"
date: 2008-09-06
comments: true
categories: [rails, windows]
---
well the last time I looked at ruby, I did it all on OpenBSD. Seemed
pretty easy at the time so I didn't make any notes. Now I'm doing it on
windows, MacOS X &amp; amaxon's EC2/ubuntu.

SQLite is no longer the default DB in rails, but for my dev PC I don't
want the whole hog. So back to SQLite it was. I consider myself a bit of
a windows guru, so I wasn't expecting any real problems. I downloaded
ruby &amp; started to get up &amp; running, installing gems, capistrano,
ec2onrails, &amp; finally sqlite3. I downloaded the SQLite3 DLL &amp;
command tool into SQLite3, added this to the path, &amp; then tried to
install the gem:

        D:\ruby\bin&gt;gem install sqlite3-ruby
        Building native extensions.  This could take a while...
        ERROR:  Error installing sqlite3-ruby:
        ERROR: Failed to build gem native extension.

        d:/ruby/bin/ruby.exe extconf.rb install sqlite3-ruby
        checking for fdatasync() in rt.lib... no
        checking for sqlite3.h... no

        nmake
        'nmake' is not recognized as an internal or external command,
        operable program or batch file.


not good!! nmake shouldn't be necessary for installing a gem. Turns out
we need to instead use:

        D:\ruby\bin&gt;gem install --version 1.2.3 sqlite3-ruby
        Successfully installed sqlite3-ruby-1.2.3-x86-mswin32
        1 gem installed
        Installing ri documentation for sqlite3-ruby-1.2.3-x86-mswin32...
        Installing RDoc documentation for sqlite3-ruby-1.2.3-x86-mswin32...


which works a treat. I expect this will trip up a fair few people so hope this helps the happy googlers.
