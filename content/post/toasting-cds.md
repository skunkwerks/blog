---
title: "Toasting CDs"
date: 2007-04-15
comments: true
categories: [mac]
---
The simplest way to create a hybrid CD is to create the image using a
directory as the source. All data is shared by default:

        hdiutil makehybrid -o /tmp/my.iso yourfolder
        hdiutil burn /tmp/my.iso -speed 8

How hard is that? The following options could also help:

- `-testburn` for doing a dry run
- `-sizequery` to see if the CD will be big enough
- `-[full]erase` for rewritable disks

manual pages are listed at [hdiutil] and also [hdid].

[hdid]: http://www.osxfaq.com/man/8/hdid.ws
[hdiutil]: http://www.osxfaq.com/man/1/hdiutil.ws
