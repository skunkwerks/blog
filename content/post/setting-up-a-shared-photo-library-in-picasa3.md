---
title: "setting up a shared photo library in Picasa3 on MacOS"
date: 2009-01-09
comments: true
categories: [mac, apps]
---
setting up a shared photo library for several users on the Mac is a piece of cake - &amp; here's the recipe. Leave me a comment if this helps you out.


1. set up a shared folder space &amp; optionally move all your existing photos into a subfolder of that
2. setup Picasa the way you want it &amp; select the shared subfolder as the only "watched folder" via Tools -&gt; Folder Manager &amp; close Picasa
3. copy your ~/Library/Application\ Support/Google/Picasa3/ folder into the shared folder space - this ensures the permissions are picked up from the watched folder
4. rename your original Picasa3 folder, &amp; set up an alias or softlink instead that points to the new folder
5. repeat #3 & #4 for each user
6. do a final permissions check, tidyup under sudo if needed

set up a shared folder space with appropriate permissions - for most people the easiest way to do this is either via chmod 777 the folder, or better, <a href="http://arstechnica.com/reviews/os/macosx-10-4.ars/8" title="inherited ACLs">understand inherited ACLs</a> &amp; use<a href="http://www.mikey-san.net/sandbox/" title="sandbox"> sandbox</a> to set up the permissions yourself. Plenty of info via google for this stuff - YMMV.
