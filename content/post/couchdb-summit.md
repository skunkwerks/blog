---
title: "CouchDB Summit in Boston"
date: 2012-04-10
comments: true
categories:
    - couchdb
---

The first CouchCamp was over 18 months ago, and since then things have
changed significantly, from the open-sourcing of [@Cloudant]'s fork of
[BigCouch], and subsequent merger, the departure of CouchDB's founder
[@damienkatz], from the project, subsequent addition of two new committers
and the recently released 1.2.0. That's a whole lot to take in!

[@sbisbee] from Cloudant whupped our raggedy butts into shape and a bunch
of core couch people hung out in Boston for 9/10 April at [@bocoup]'s
nice offices for the CouchDB Summit, two days (and nights) of pure couch.
Coincidentally, Bocoup also have a Red Couch. Significant!!

The get-together was long overdue to spend some quality time talking
CouchDB & Couch stuff, and while it was a great opportunity to see what
apps and functionality people had been building in and around CouchDB,
the real value was to talk about features & roadmaps, making Couch more
accessible to the community including packagers, maintainers, users, and
developers. I have renewed confidence in both the community, and the code
that is going to come out of the project.

You'll see more of this both on my blog, on the CouchDB [mailing list],
and also soon in my new [@CouchDBWeekly] newsletter/blog/propaganda/evangelism
site - soon to come.

Finally, huge kudos to [@sbisbee], [@bocoup], and [@cloudant] for organising
us, sharing your home with us, and helping fund this!

While I was there, I got to see some fantastic things are being done with
CouchDB now, both client-side on smartphones and tablets, and also with
back-end plumbing in and around node.js. I could have spent a week just
listening to people and discussing and learning about the smart things
they're doing with CouchDB. Some links for you to chew on are [kan.so],
and two hot forks of CouchDB, [refuge] and [pouchdb]. Inspiring stuff!

Also, version 1.2.0 hit the mirrors last week with a bang, and the internet
resounded with tweetz. This release has been a _long_ time cooking,
and it is chock full of goodies to make even the most hardened SQL fanatics
take a second look. We feel it's a validation of the Apache Way, and
demonstrates the value of community over code in developing and
delivering high-quality software. And we think you'll like the result -
check it out on our [new site]!

[@dch]: http://twitter.com/!#/davecottlehuber
[@sbisbee]: http://twitter.com/!#/sbisbee
[@cloudant]: http://twitter.com/!#/cloudant
[@bocoup]: http://twitter.com/!#/bocoup
[BigCouch]: http://bigcouch.cloudant.com/
[@damienkatz]: http://twitter.com/!#/damienkatz
[mailing list]: http://couchdb.apache.org/#mailing-list
[@CouchDBWeekly]: http://twitter.com/!#/couchdbweekly
[kan.so]: http://kan.so/
[pouchdb]: http://arandomurl.com/2012/03/27/pouchdb-is-couchdb-in-the-browser.html
[refuge]: http://refuge.io/
[new site]: http://couchdb.apache.org/

