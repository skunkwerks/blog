---
title: "agile meetings"
date: 2007-11-10
comments: true
categories:
    - meetings
---
I _know_ there's a better way of running those meetings but I haven't
found it ... yet. So here's something that might just help out -
[Scrum] meetings are all about getting everybody on board, feeling the
commitment on a day-to-day basis.

I hope to put this in place over the next few weeks and see how things pan out.

[scrum]: http://www.mountaingoatsoftware.com/scrum
