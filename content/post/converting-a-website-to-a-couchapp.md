---
title: "Converting a website to a CouchApp"
date: 2012-06-13
draft: true
comments: true
categories:
---

I saw a beautiful data [visualisation] on Santiago Ortiz' blog but realised
he was loading the raw data each time from comma and tab separated data
files into JavaScript before creating the visualisation.

I contacted him, and he's more than happy to have his fine work abused
and reworked to sit on the couch. Woot!

First up was to scrape the site, given there are only a few files required
I just did this using a killer combo of Chrome Dev Tools to see what XHR
requests were required, and then `wget`ted them to disk.

The next piece of work is to wrap a couchapp around this.


[visualisation]: http://moebio.com/datavisnetwork/
[@moebio]: http://twitter.com/moebio
