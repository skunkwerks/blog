---
title: "WP as site root"
date: 2007-03-25
comments: true
categories: [openbsd,wordpress]
---
About 3 months ago, I was trying to get WP to run my whole site -- but
still allow access to existing pages. This seemed easy, using the new
features of WP2.1 but I couldn't seem to get the right apache magic.
Recently, a few other people have had similar issues (check out WP support
forums) and finally the last bits of the puzzle fell into place. It's a
mix of working with the chroot magic on OpenBSD and also correct settings
for WP itself. The recipe follows: I have a symlink from
`/var/www/htdocs/wordpress` back to the original `/var/www/wordpress`
directory that I sync regularly from the subversion repository.

Create `/var/www/htdocs/.htaccess` with following content:

        # BEGIN WordPress
        <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteBase /wordpress/
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule . /wordpress/index.php
        </IfModule>
        # END WordPress

And then ensure permissions are sorted out appropriately:

        chown root:daemon /var/www/htdocs/.htaccess
        chmod 0644 /var/www/htdocs/.htaccess

But that's still not quite enough to have it run under OpenBSD's chroot.
By default, the permissions placed on `/var/www/htdocs` by `httpd.conf`
prevent WP from performing the rewrite magic, specifically disabling file
tests. The way Apache knows whether to direct pages to WP or to the file
you asked for is simple - if a file or directory exists with the name
you asked for, it will be served up; if nothing exists then your request
gets handed over to WP first. So, here are the changes needed to
`/var/www/conf/httpd.conf`:

        # This controls which options the .htaccess files in directories can
        # override. Can also be "All", or any combination of "Options", "FileInfo",
        # "AuthConfig", and "Limit"
        #
        # AllowOverride None
        AllowOverride FileInfo

And then you can set up where you want your blog to appear under WP options
menu as usual.
