---
categories: [freebsd, infrastructure, networking, reliability]
date: 2016-10-20T12:34:37+02:00
keywords: [haproxy, carp, freebsd, robustness, reliability]
draft: true
title: "CARP: the dawn of a new era"
---

> Idempotent Repeatable Composable Loosely Coupled Infrastructure is my motto
> -- me

In the spirit of loosely coupled infrastructure, I've been working on
separating out the various layers.