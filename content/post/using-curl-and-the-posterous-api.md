---
title: "Using cURL and the Posterous API"
date: 2011-12-03
comments: true
categories: [cURL,API,Posterous, migration]
---
Posterous has a really neat feature of their API docs - you can use it directly
from the web page. Unfortunately, I only need it to migrate off to Octopress.

Log into Posterous and then open the [API] page. Use the first entry to
obtain your API token. Put this, your login and password below and you should
be able to obtain a list of your Posterous Sites, or Spaces as they're now
called. <!--more-->

    $ curl --silent -vX GET --user you@domain.com:passwd \
      -d "api_token=your_API_token" http://posterous.com/api/2/sites
    
    * Connected to posterous.com (184.106.20.99) port 80 (#0)
    * Server auth using Basic with user 'you@domain.com'
    > GET /api/2/sites HTTP/1.1
    > Authorization: Basic BEEFDEADXNlLm5ldC5uejp3dWJ3dWI=
    > User-Agent: curl/7.21.4 (universal-apple-darwin11.0) libcurl/7.21.4 \
            OpenSSL/0.9.8r zlib/1.2.5
    > Host: posterous.com
    > Accept: */*
    > Content-Length: 42
    > Content-Type: application/x-www-form-urlencoded
    > 
    } [data not shown]
    < HTTP/1.1 200 OK
    < Server: nginx/0.7.65
    < Date: Tue, 13 Dec 2011 00:49:49 GMT
    < Content-Type: application/json; charset=utf-8
    < Connection: close
    < Status: 200 OK
    < X-RateLimit-Limit: 1000000
    < 
    { [data not shown]
    * Closing connection #0

Lovely. Now that will also provide you with some output, deep inside
that is a `site_id` field - your site. I've conveniently relabelled
this one as `1234567`:

    {
          "is_private" : false,
          "hostname" : "your_posterous_site_name",
          "readers_count" : 1,
          "admins_count" : 1,
          "full_hostname" : "your_posterous_site_name.posterous.com",
          "profile_image_35" :
            "http://files.posterous.com/user_profile_pics/1357443/dch_thumb.jpg",
          "admins" : [{
                "current_user_following_info" : null,
                "firstname" : "Dave",
                "nickname" : "dch",
                "site_id" : 1234567
            }]
    }

Take that `site_id` and bung it into this curly command, and stash your
posterous content to a file. Note how I use the `pages=2` parameter
to retrieve the subsequent page.

    $ curl --silent -X GET --user user@domain.com:passwd -d \
      "api_token=DeaDBeeFDogFooducDdoG" \
      http://posterous.com/api/2/sites/1234567/posts > /tmp/posts1.json
    $ curl --silent -X GET --user user@domain.com:passwd -d \
      "api_token=DeaDBeeFDogFooducDdoG" \
      http://posterous.com/api/2/sites/1234567/posts\?pages\=2 > /tmp/posts2.json

In particular, note the headers returned by cURL - this is a simple
validation that we're on the right page, and have recovered all
the data. Great!

    < X-Current-Page: 2
    < X-Runtime: 29
    < Content-Length: 148638
    < X-Total-Entries: 31
    < X-Per-Page: 20
    < X-Total-Pages: 2


For bonus points, next time I'll use CouchDB's awesome powers to convert
this back into octopress format.

[API]: http://posterous.com/api
