---
title: "old dog learns new tricks"
date: 2010-07-06
comments: true
categories: [couchdb, apps]
---
I've always been interested in scalability and simple solutions. For the last ten years I've worked mainly in managed storage services, for large enterprises and telcos. Nothing could be further from scalability and simplicity in those environments. I've watched as clients poured millions of dollars and euros into badly architected apps, to thrust redundancy and reliability on them through hardware. But it's just too late by then to fix anything.</p>
The last serious programming I did was in perl, more than a decade ago - last century even - and it shows. I remember migrating from perl4 to perl5. At the time perl was cutting-edge for web development, with the CGI module rising with the then new apache webserver. Things have moved on again since then, and the new web is based on RESTful APIs, javascript, jquery, presented through web browsers. About 9 months ago I ran into CouchDB serendipitiously. Let's just say it caught my fancy. A couple of books later I am wombling through building my own applications, that can scale indefinitely without re-architecting, &amp; won't require enterprise-grade compute and storage to run. On top of that it's great fun to be learning while working with cool stuff again.</p>
This series of posts is focused on the slow learning curve of teaching this old dog some new tricks. In particular I'm looking at <a href="http://couchdb.apache.org/" title="CouchDB">CouchDB</a>, using <a href="http://couchapp.org/" title="CouchApp">CouchApp</a>, javascript and later on the rapidly moving <a href="http://couchapp.org/couchapp/_design/couchapp.org/vendor/couchapp/docs.html#/">Evently framework</a>. If like me you are just starting out with all of these, you'll be running into a few headaches in just getting some basic things working. Here's hoping this helps you out.</p>
If you want a head start &amp; some amusement check out all the relax-ing videos below</p>

- Learning to [Relax](http://video.yahoo.com/watch/5019677/13353143)
- [Advanced CouchDB](http://techportal.ibuildings.com/2010/01/05/advanced-couchdb/)
- as well as any videos by J Chris Anderson ...

Finally two key couch resources are:

- the [Definitive Guide](http://books.couchdb.org/relax) to CouchDB - some
 of the sections (templates, building an app) are a little dated now due
to version creep but it's still a great book.
- the [wiki](http://wiki.apache.org/couchdb/)
- not to mention the mailing list, as well as the #couchdb irc channel

