---
title: "cygwin dumps core on Windows 2008 R1SP2 on EC2"
date: 2010-08-11
comments: true
categories: [cygwin, ec2, windows]
---
I have been trying to speed up the process of building CouchDB on Windows
by having a high-speed pre-prepared Amazon EC2 image. Microsoft's VC++,
mozilla-build with msys, and cygwin tools are required to build erlang and
then later on, CouchDB which also uses erlang. So, no cygwin = no erlang
= no couchdb = showstopper!

If you are trying to install cygwin or similar tools on Windows 2008 R1SP2
Datacenter Edition, either 32 or 64-bit images, you&rsquo;ll likely not even
be able to complete the installation. bash dies regularly, the cygwin
installer spits out errors faster than an uncapped BP oil well. This bug
affects mozilla build tools, cygwin bash and other tools, such as the
msys/mingw compiler as well. Apply the reg fix from
[KB956607](http://support.microsoft.com/kb/956607), reboot and re-install
any tools to ensure their final installation tweaks complete successfully.
This still hasn' been sufficient for me but I am getting further than before.

Corinna on the [cygwin](http://omgili.com/mailinglist/cygwin/cygwin/com/AANLkTi=xgJzA5-pMz2mkg-b0CVf4PwFeYCy3RfA4NHgfmailgmailcom.html)
 list has confirmed that this issue does not recur on a physical install
- thanks! So this is either an EC2 tweak or a Xen bug. While I've not
tested this, it is possible that the older AMD chipset Xen hosts will run
successfully, vs the newer Intel x86_64 chips which don't.

I will be following this up with
[Amazon](http://developer.amazonwebservices.com/connect/thread.jspa?messageID=190190).
