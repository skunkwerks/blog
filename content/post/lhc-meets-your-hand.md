---
title: "LHC meets your hand"
date: 2010-09-24
comments: true
categories: [humour]
---
love it - what happens when you put your hand into the [LHC]?
Scientists respond, from the quite reasonable through to the delightfully
absurd. Does help if you have some physics background.

[lhc]: http://kottke.org/10/09/putting-your-hand-in-the-large-hadron-collider
