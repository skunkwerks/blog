---
title: "Building the first CouchDB Release"
date: 2010-07-09
comments: true
categories: [couchdb, release]
---
Building the first ever CouchDB release coincided nicely with a complete
fresh Snow Leopard install due to a dead disk. So this time I've tried out
[homebrew] instead of macports as recommended by jan@

    brew install erlang
    brew install icu4c
    brew link icu4c

Then, to install CouchDB normally you'd just go ahead and
`brew install couchdb`... and relax. But for a release it's different;
download the [bits](http://home.apache.org/~nslater/dist/) and:

    cd /tmp
    tar xvzf ~/Downloads/apache-couchdb-1.0.0.tar.gz
    cd apache-couchdb-1.0.0.tar.gz
    ./configure --prefix=/tmp/couchdb
    make
    make check
    make install
    pushd /tmp/couchdb/bin
    ./couchdb &

Then go to the browser-based [test suite] to check its all good.

[homebrew]: http://github.com/mxcl/homebrew
[test suite]: http://127.0.0.1:5984/_utils/couch_tests.html?script/couch_tests.js
