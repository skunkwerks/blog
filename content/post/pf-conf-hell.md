---
title: "pf.conf hell"
date: 2007-03-31
comments: true
categories: [openbsd, pf]
---
So far, I've not been able to get packets to flow across the pf.conf
interface. Better luck another day! I can see that they're getting
blocked immediately by `rule0` but no way to work out what `rule0`
actually is.
