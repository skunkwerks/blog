---
title: "crashdump analysis on windows"
date: 2008-07-24
comments: true
categories: [windows, debugging]
---
after spending a couple of hours debugging my brother-in-law's crashing
computer (in german, just to make it easier), I ran into this [site]
which is so good I might just have to buy a few of the books listed on it.

[site]: http://www.dumpanalysis.org/
