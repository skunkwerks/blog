---
title: "Putting the Apache back into CouchDB"
date: 2012-02-04
comments: true
categories: [couchdb, asf]
---
Over the last year, the Apache CouchDB project feels like it's been in a
holding pattern between missing [Benevolent Dictator] and [ASF]-style
[meritocracy].

It hasn't helped that Damien Katz, the original visionary developer of
CouchDB, has been largely absent in the community since the CouchOne and
MemBase merger. And that's a shame - few know CouchDB better than him.

His blog post about the future of [CouchBase] comes as no surprise to
anybody who followed the merger; CouchOne was funded by venture capital,
and as such the owners will be pushing for a return - and there's nothing
wrong with following the money.

## Relevance


> An important goal is to keep interfaces in CouchDB simple enough that
> creating compatible implementations on other platforms is feasible.

 -- @DamienKatz [FAQ about CouchDB and its new IBM overlords](http://damienkatz.net/2008/01/faq_about_couch.html)

Despite that, I still think that Apache CouchDB is not only relevant,
but indeed essential in the increasingly internet-connected world. The
trend is for people to put their personal data into a corporate cloud,
where it is sliced, diced and ultimately resold. For most users, there's
simply no alternative, and CouchDB is to my mind the best way to develop
a peer-to-peer internet.

I'd like to see CouchDB as being the enabler in open data, in breaking
open the web for joe & jane user, and enabling interoperability of large
data sets especially in research and for government. And an
independent replication protocol, with a reference implementation,
well supported and documented, is critical to that end - even if the
back end isn't actually CouchDB.

## Community and Commitment

The points made by both [Mikeal] and [Damien], and others, are valid,
however. CouchDB needs to have both focus and drive, to keep up its active
current developer base, and to grow the user community. And it needs to
happen this year or it will likely be too late.

## Healing the Wounds

So where to from here? What's really needed? The top IRC peeves are not
too hard to address:

- Inability to install or build on common platforms such as Ubuntu, Debian
- Finding appropriate documentation across the wiki, API, and the
out-of-date Definitive Guide

But clearly that's not sufficient -- in terms of restoring confidence,
and increasing mind-share, things are not so straightforwards.

Fundamentally the Apache CouchDB community needs to get it together --
to start demonstrating progress to the wider coder community, and do
so in a way that articulates the project's objectives, and restores
faith that the Apache Incubator process really did create a community
around the code, that can survive the clumsy departure of its inventor.
Here's a suggestion:

## Communicate the Way Forwards:

- Have an objective - what's Apache CouchDB all about? Why should I use it?
- An awe-inspiring Road Map, and a way for people to influence it
- Predictable and reliable release schedule

## Make it easy to get started:

- Clean up the wiki and main landing page
- Pre-built binaries to help people get up and running quickly on common
	platforms like Debian and Ubuntu
- Closer links to package maintainers
- Bring the Definitive Guide up to date with current CouchDB functionality

## Continue to Build the Community:

- Tell a story around the rich Couch eco-system
- Have a Propaganda Pack to help people evangelise Apache CouchDB
- Do a weekly update on what's happening in CouchDB world

Interestingly enough, there's not a lot of code required, but there is
a fair bit required from the community.

This is a great opportunity for people who'd like to contribute, and
who are keen to learn more about CouchDB along the way.

Are you [joining me](mailto:user@couchdb.apache.org)?

[mikeal]: http://www.mikealrogers.com/posts/apache-considered-harmful.html
[couchbase]: http://blog.couchbase.com/couchbase-commitment-to-open-source-and-couchdb
[touchdb]: http://nosql.mypopescu.com/post/15294965548/touchdb-embeddable-lightweight-couchdb-compatible
[damien]: http://damienkatz.net/2012/01/the_future_of_couchdb.html
[merger]: http://www.couchbase.com/press-releases/membase-couchone-merge
[Benevolent Dictator]: http://en.wikipedia.org/wiki/Benevolent_Dictator_For_Life
[meritocracy]: http://communityovercode.com/over/
[ASF]: http://apache.org/foundation/faq.html
