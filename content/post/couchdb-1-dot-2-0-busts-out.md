---
title: "CouchDB 1.2.0 Busts Out"
date: 2012-04-08
comments: true
categories:
    - couchdb
---

In other news today, a warrant was issued for the arrest of Apache
CouchDB 1.2.0. An [unnamed source] repo points the finger at a steady
supply of dangerous performance-enhancing patches, including
[Google Snappy], a high-speed compression engine applied at the
document level, and the infamous [YAJL] streaming JSON parser, optimised
for performance even in low memory situations.

A closer examination of the source confirms that these patches were
applied in native form using Erlang NIFs, and as a result, users are
warned to expect dizzying increases in overall throughput with
now-native JSON parser. Subsequent usage of compaction of views and
databases will result in permanent weight loss. This change requires an
upgraded on-disk format.

CouchDB 1.2.0 is full of other improvements including a tunable
replication engine, providing both improved throughput and also tweaks
for specific use cases.

Due to changes in CouchDB's security model, detectives have been unable
to retrieve hash user passwords. Further investigation is highly
recommended, and you can read more at our [official blog].

No upholstery was harmed during the making of this post.

[Google Snappy]: http://code.google.com/p/snappy/
[YAJL]: http://lloyd.github.com/yajl/
[unnamed source]: http://git-wip-us.apache.org/repos/asf?p=couchdb.git;a=blob_plain;f=NEWS;hb=1.2.0
[official blog]: https://blogs.apache.org/couchdb/entry/apache_couchdb_1_2_0
