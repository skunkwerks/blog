---
categories: [freebsd, jails, diy]
date: 2017-02-17T21:31:52Z
keywords: [freebsd, jails, diy]
draft: true
title: DIY Jails - laying bare the very fabric of FreeBSD
---

## Prologue

FreeBSD jails have been referred to as "Docker Done Right" which is a bit unfair
as Docker brings a number of things that FreeBSD jails haven't had directly - a
large pool of ready-made apps and tools that you can just drop in, and simple
scripting and networking primitives to bind all that together, but we'll also
see that there's some merit in the jibe.

Given that I'm writing an article about DIY Jails you can probably guess by now
that I'm going to take that vague statement above and lay it out in all its
naked glory for us to see.

It's also no secret that I am a *huge* fan of tools like [iocage] and [iocell],
but I did wonder just how the magical sausage was made. From my experiences with
Docker and other tools, I expected it would be a long hard road, with poor
documentation and arcane invocations of cryptic commands.

Fear not, though, brave reader, for I am here with you and will guide you on our
dark journey into the bowels of that Daemonic Operating System, FreeBSD...

## Imagine

In an ideal world, creating a container from scratch would be as easy as
unpacking a copy of your chosen operating system, tweaking a few files related
to networking, and ... that would be it.

I started off browsing the FreeBSD man pages including [jail(1]) and the [jail]
section of the FreeBSD handbook, and I became concerned - a few short pages?
Surely there was more to it than that? Aah yes, of course, there's
[jail.conf(5)] too. Also mercifully short, a bit too short as we will see.

## To work









[iocage]: http://iocage.readthedocs.io/
[iocell]: http://github.com/bartekrutkowski/iocell
[jail]: https://www.freebsd.org/doc/handbook/jails.html
[jail(1)]: https://www.freebsd.org/cgi/man.cgi?query=jail&sektion=1
[jail.conf(5)]: https://www.freebsd.org/cgi/man.cgi?query=jail.conf&sektion=5

