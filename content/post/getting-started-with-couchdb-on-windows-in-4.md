---
title: "Getting started with CouchDB on Windows in 4 easy steps"
date: 2010-08-03
comments: true
categories: [couchdb]
---
Not only is CouchDB fun &amp; relaxing, it&rsquo;s also really easy to get
started. Those nice Cloudant poeple are providing free couch hosting to help
you get started at [cloudant.com](http://cloudant.com/)

## Getting into CouchDB on Windows

So you just can&rsquo;t wait to relax can you? The fastest route is just 4 easy steps to CouchDB zen:

Install the bits; 32-bit versions only

- download &amp; unzip `curl` from [http://curl.haxx.se/]
- grab the [couchapp](http://benoitc.github.io/couchapp/) installer
- sign up for couchdb hosting as above

## Relax

- read the [couchapp quick start notes](http://github.com/couchapp/couchapp/blob/master/README.md)
- for more info on CouchDB you can read the [Definitive Guide](http://books.couchdb.org/relax/)
- or check out the wiki with more info on running with [Windows quirks](http://wiki.apache.org/couchdb/Quirks_on_Windows)
