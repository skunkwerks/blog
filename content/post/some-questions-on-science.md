---
title: "some questions on science"
date: 2008-07-26
comments: true
categories: [musing]
---
over on [Thinking Matters] there's a whole lot of discussion around
"what is science", &amp; "where can we draw boundaries between it and religion".
For me that nicely illustrates the difference between the two domains -
science is inclusive, inquisitive, and able to be challenged, downsized
and redirected - think flat earth, newtonian physics for examples of
fundamental changes in the way we view the world that science has
permitted and incorporated.

Specious teleologic arguments are used to imply that religion is separate,
special, and should be treated outside this evidential framework. Fair
enough if that's your view, but I've yet to see why this is required,
and why it is allowed to be used to "explain" what has already been
explained by science.

evolution is the best example of where these worlds collide. Scientifically
the world has accepted the Darwinian concept of evolution, despite some
tinkering of how fast &amp; how discrete the steps are by the likes of
Steven Jay Gould and his opposing immovable force Richard Dawkins. The
factual record is extensive &amp; consistent. So when we review the age
of the earth (6000 years give or take) from a biblical perspective, we are
expected to put Occam's razor to the side, and invoke a mysterious being
(not seen for the last 2000 years in any verifiable form) who compiled
all of this evidence, and asks us to ignore it.

If this approach was used anywhere else, for example in a court of law,
you'd deem it unacceptable. So why put up with it here?

[Thinking Matters]: http://thinkingmatters.org.nz/2008/07/some-questions-on-science/
