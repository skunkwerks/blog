---
title: "ubuntu saves the day"
date: 2010-10-28
comments: true
categories: [admin, linux]
---
My work laptop had a BSOD today, which looks like it was caused by bit rot
on the root partition. While everything's backed up onto S3, restores from
New Zealand of 20GiB of data take a while, so I was kinda hoping to recover
smoothly without getting the IT goons to visit, who probably will just
rebuild it. I'm pretty sure that's fair payment for getting chocolate
inside my laptop's fan.

It was a good opportunity to give ubuntu maverick a spinup before it goes
on the big iMac at home as dual-boot. The install is slowly tweaked each
time, and it's really clean. I am pretty sure my mum could do this without
any help now, and the fresh look is nice - it's truly a class act OS.

One workaround was needed to resolve what is probably a stuck [trackpad]
on the loan laptop; with a `rmmod psmouse` then it was all go. Everything
works which really is an impressive step forward for Canonical, with strong
OEM relations clearly now paying off. Hats off team!

Anyway long story short, nautilus and brasero to the rescue, and I now have
a bunch of md5 checksummed DVDs stashed before the hired goons come
tomorrow to blow it away. I love the ntfs integration in linux, and the new
maverick Ubuntu gets thumbs up all round - especially as it's now got
CouchDB 1.0.1 included - yay!

[trackpad]: http://xpapad.wordpress.com/2009/09/09/dealing-with-mouse-and-touchpad-freezes-in-linux/
