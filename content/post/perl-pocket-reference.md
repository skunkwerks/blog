---
title: "Perl Pocket Reference"
date: 2012-05-05
comments: false
categories: [perl, reviews]
---
Perl Pocket Reference is exactly that, and no more - or less however!

This 5th Edition is a venerable update to one of the web's most
transformative programming languages. Perl's regex have become endemic
(some would say advisedly so) in every language since, and it pioneered
coming to terms with internationalisation and unicode in the browsable
internet.

The format hasn't changed much but the coverage brings it up to date with
5.14 again, and is a great value addition to your indispensable Llama and
Camel books that you've no doubt already got. While much of the material
is arguably available from terminal via perldoc, or the [online equivalent],
you can't beat flicking through this even from the keyboard. It's
terse style leads you straight to the information you need, without frills.

Overall: If you are not a full-time perl developer, or have intermediate
skills, you'll find yourself flicking through this time & time again to
confirm just how that specific function does work. The electronic versions
are conveniently supported with relevant links to forums and online docs
meaning you are never far away from further information if you need it.
It's greatest challenge is the ubiquitous search engine, but at this price
it's arguably great value.

This review was brought to you by the O'Reilly [blogger programme].

[blogger programme]: http://oreilly.com/bloggers/?cmp=ex-orm-blgr-dave-cottlehuber
[online equivalent]: http://perldoc.perl.org/
