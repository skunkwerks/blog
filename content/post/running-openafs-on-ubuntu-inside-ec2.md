---
title: "running OpenAFS on Ubuntu inside EC2"
date: 2008-06-01
comments: true
categories: [openbsd, ec2]
---

first up, kick off an EC2 ubuntu/hardy instance, ssh in as usual

    apt-get install openafs-fileserver
    # set cell name to muse.net.nz
    # set cache size to 10Gb
    # set cellDB to afsdb.muse.net.nz
    # update /etc/openafs/CellServDB
    &gt;muse.net.nz            #home
    121.73.27.12                    #afsdb.muse.net.nz
    rm /etc/openafs/server/CellServDB
    ln -s /etc/openafs/CellServDB /etc/openafs/server/CellServDB
    # sweet!
