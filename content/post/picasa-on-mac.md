---
title: "Picasa on Mac"
date: 2009-01-09
comments: true
categories: [mac, apps]
---
google released [Picasa] 3 today for MacOS - timely indeed as I've gotten
frustrated with iPhoto 6 `hate hate hate` needing to manage/import 20 000 tagged
and sorted photos from my PC (with Picasa) onto my Mac (with iPhoto), and
have been looking at Aperture 2.0 or Expression Media as an alternative.

A few things struck me immediately -

- Picasa3 on my iMac is <em>really</em> fast for sorting and scrolling through
 photos - admittedly on an intel dual core 2.16GHz proc with 3 GB RAM
- it looks pretty good for a first release and is easy to use "out of the box"
- importing photos and emailing them with your google account is really
 easy - this will make my wife really happy
- unlike the PC version there's no trail of picasa.ini files spattered all
 over the filesystem

A more detailed test run uncovered a few faults -

- tagging images sucks - you can't easily see the tags all the time as
 it's a pop-up window only & needs to be recalled each time
- my apple remote doesn't work in slideshow view but my bluetooth keyboard
 and mouse are a fair compromise :-)
- on importing photos, the "delete from camera" option doesn't seem to work
  maybe PEBKAC but I don't think I made any mistakes
- something isn't quite right in the "import and rotate" image workflow;
 I think the preview doesn't show the image with the camera's correct
  horizontal/vertical alignment, but the final imported version is OK

Conversely, the learning curve is short and sweet, and it was easy to set
up a shared library between my wife's account and mine on the same Mac
- more on that in another post. For me this is good enough to hold off
from getting Aperture for another few months - which has been a really
nice app to use BTW just a big sluggish in the image department.

Above all, unlike that  like that crappy iPhoto 6 `hate hate hate` it didn't
screw up my current photo library at all.

[picasa]: http://googlemac.blogspot.com/2009/01/picasas-macworld-debut.html
