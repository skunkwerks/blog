---
title: "Aperture 3.03 unsupported image format"
date: 2010-07-10
comments: true
categories: [mac,apps]
---
After losing my hard drive on my iMac I had a fresh install of Snow
eopard & a spanking new disk too. I installed AP3.03 & opened my old
library. The most recent photo shoot, from our trip to Rarotonga, is
now reporting "Unsupported Image Format" for over 180 jpegs. You can
see them on disk, the preview.app can read them -- just AP3.03. Crap.
If I export as a new library, then duplicate, they come right - but I
need a better solution for the remaining 73000 photos. Apple your premier
photo product has had a bad run - is AP3 worm-ridden to the core?
