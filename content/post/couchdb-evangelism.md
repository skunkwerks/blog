---
title: "CouchDB Evangelism"
date: 2012-04-19
comments: true
categories:
    - couchdb 
    - evangelism
---
In an earlier post I made a commitment to evangelise CouchDB more. I'm
making good progress on this so far:

- another [CouchHack] has been set up in Vienna, Austria - sign up!
- I'm giving a talk about CouchDB 1.2.0 and friends at [ViennaJS] soon
- the planned [@CouchDBWeekly] newsletter got derailed but will be out this Friday
- the CouchBase documentation import is underway and should be available for 1.1.0 next week

Couch is an awesome platform, and with a set of killer features such as
replication, inbuilt webserver & database, and incremental map-reduce,
it should be an enabler of open data and a great environment to use to
learn how the internet works for new programmers and engineers. So it's
important to get out there and make it happen!

[CouchHack]: http://www.couchhack.org/
[@CouchDBWeekly]: http://twitter.com/!#/couchdbweekly
[ViennaJS]: http://viennajs.org/
