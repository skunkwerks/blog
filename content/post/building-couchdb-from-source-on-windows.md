---
title: "Building CouchDB from source on Windows"
date: 2010-09-08
comments: true
categories: [couchdb, erlang, windows]
---
Here's the first post of a series on getting CouchDB built from source on
Windows. This one focuses on getting all the compilers sorted out - the
most cumbersome part of the process. Let's start with Erlang as it's the
core of CouchDB.

Building Erlang & CouchDB on Windows requires a custom build environment,
which is very sensitive to path order amongst the three different compilers
used to build wxWidgets, Erlang, JavaScript, and CouchDB.

This is further complicated by different install locations on 32 vs 64
bit windows versions, and which Microsoft C compiler and Windows SDKs are
installed.

Each component is described via Makefiles in the Cygwin unix/posix
emulation layer, and then handed over to the appropriate compiler to run.

## Cygwin

The full Cygwin install comprises several GiB of data. Run
[setup.exe](http://www.cygwin.com/setup.exe) using defaults, optionally
installing all components if you have the bandwidth, or alternatively with
the following additional modules at a minimum:

- devel: ALL
- editors: vim
- utils: file

We'll be using the new shell icon on a regular basis from here on in.

## Mozilla Build

- The mozilla build toolchain is needed solely for building our javascript engine.
- download it from [mozilla](http://ftp.mozilla.org/pub/mozilla.org/mozilla/libraries/win32/MozillaBuildSetup-Latest.exe)
and install per defaults.

## Microsoft Visual C++

Erlang and CouchDB can be built using the free VS2008 Express C++ edition
from [MSDN](http://msdn.microsoft.com/en-gb/vstudio/) if you don`t already
have a full VS2008 commercial released install You'll only need to install
Visual C++ 9 only, to the default locations, either using the [DVD
ISO](http://download.microsoft.com/download/E/8/E/E8EEB394-7F42-4963-A2D8-29559B738298/VS2008ExpressWithSP1ENUX1504728.iso),
or the smaller MSVC Webstart. You can exclude the optional MSSSQL &
Silverlight components. I've not tried compiling CouchDB + components using
Visual Studio 2010 yet as the project file and build tool formats have
changed again - but feel free to let me know if you get it working.

## Windows 7 SDK


The (free) windows 7 SDK is required, as the free VS2008 install is missing
the message compiler. Download one of the following version per your
requirements & install.

- [intel 32bit](http://download.microsoft.com/download/2/E/9/2E911956-F90F-4BFB-8231-E292A7B6F287/GRMSDK_EN_DVD.iso)
- [amd64](http://download.microsoft.com/download/2/E/9/2E911956-F90F-4BFB-8231-E292A7B6F287/GRMSDKX_EN_DVD.iso)


## Getting the environment right

After installing VC++ 2008 Express, there is a new batch file which
correctly sets up the environment for you.
`"%vs90comntools%\..\..\vc\vcvarsall.bat" x86` will automatically find
the correct path, and set up our 32-bit build environment correctly,
independently if you have installed on 32 or 64bit windows. I set up a
script to do this for me & then launch a cygwin shell directly:


    @echo off
    call "%vs90comntools%\..\..\vc\vcvarsall.boat" x86
    pushd C:\cygwin\bin
    bash --login -i

In your new cygwin prompt look at your path `echo $PATH | sed 's/:/\n/g'`
and check ther's nothing missing or unexpected. In the next post, I&rsquo;ll
include getting Erlang/OTP set up ready for compilation<
