---
title: "Great Intro to jQuery Mobile"
date: 2011-08-17
comments: true
categories: [jquery, reviews]
---
jQuery Mobile is the latest addition to the reliable and popular jQuery
framework family. It's rapidly evolving - just entering 1.0 beta2 as I
write this post. It's not a huge book - just 6 chapters - but covers the
main points:

1. overview
2. paging, dialogs, ajax and history
3. core UI elements - toolbars, buttons, lists, forms, grids
4. theming
5. the new jQM events and methods, and customisation
6. putting it all together by building a twitter app from scratch

There are some good quick tips thrown in throughout the book, bringing
some practical experience to help newcomers, and a few bones for more
experienced people as well. The first third (paging, dialogs, ajax and
history) are straightforwards and anybody already familiar with HTML of
any era, and basic jQuery syntax, should eat this up. At this point a
basic jQM web app is easily within reach of most people, as the author
provides plenty of examples with code and screenshots.

The latter stages of the book pick up the pace quickly, and requires more
than a cursory understanding of HTML and Javascript/jQuery. A number of
tips focus on the underlying event model & while this is of interest, it
is also well covered on the official jQM website now. The author takes
the docs further and shows how to integrate these models into useful
code such as handling changed orientation, or enabling swiping to produce
page transitions. He finishes up with a sample Twitter app, pulling what
was covered in the book nicely.

Overall: If you are not a full-time mobile developer, or have intermediate
HTML and Javascript, ideally jQuery, experience, this book will both get
you started, and give you a head start on applying core mobile jQuery
functionality. It's a practical book representing good value at its price
point, and seems to have survived the pre-1.0 changes in jQM well, and
will be something I refer back to for a refresher each time I pull a new
mobile app together.

This review was brought to you by the O'Reilly [blogger programme].

[blogger programme]: http://oreilly.com/bloggers/?cmp=ex-orm-blgr-dave-cottlehuber
