---
title: "A fresh start with Snow Leopard Mac OS X b10.6.4"
date: 2010-07-09
comments: true
categories: [mac,apps]
---
A list of twenty things I forgot the last time from installing Snow Leopard

## Key Apps

- Transmission
- Komodo
- MailPlane
- KeePassX
- iTerm
- Aperture
- Firefox + jsonview + adblock plus + sync + firebug + `about:config exportHTML=true`
- QuickSilver
- Growl & GrowlTunes
- JungleDisk
- EyeTV
- vmWare Fusion
- Skype
- VLC
- Colloquy

## Tweaks

- [Deep Sleep](http://www.axoniclabs.com/DeepSleep/)
- enable disk-based hiberation with `sudo pmset -a hibernatemode 1`
- use to find out what slows things down use this: `pmset -g log`

## Build Tools

- XCode no surprises here
- Install brew then [sort out] perl and gems
- brew install git, rsync, python
- use pip not easy_install

[sort out]: http://wiki.github.com/mxcl/homebrew/gems-eggs-and-perl-modules
