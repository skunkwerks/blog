---
title: "Running IRC Meetings with MeetBot"
date: 2012-10-15
comments: true
categories: [python, irc, meetings]
---

Running meetings is a black art at the best of times. Running them over
IRC is a step more complex. Instead of audio, and people waiting turns,
you've got a thousand monkeys typing a thousand thoughts. Async. On
recommendation from @nslater's involvement in another project, I gave
[MeetBot] a try. It's based on a venerable Python IRC bot called [supybot],
and while setup on debian is a piece of cake, it certainly wasn't so
obvious on my mac. Here's how:

## Installing SupyBot

Download supybot's [source] and unpack it somewhere handy. Grab [meetbot's source] too.

    pushd /tmp
    wget http://garr.dl.sourceforge.net/project/supybot/supybot/Supybot-0.83.4.1/Supybot-0.83.4.1.zip
    wget http://code.zgib.net/MeetBot/MeetBot-current.tar.gz
    unzip SupyBot-0.83.4.1.zip
    tar xvzf MeetBot-current.tar.gz
    mv MeetBot-current/ircmeeting MeetBot-current/MeetBot/
    pushd

Now we're ready to install SupyBot along with required plugins.

    mkdir ircbot && cd ircbot
    virtualenv ircbot
    source ircbot/bin/activate
    which python pip
    pushd /tmp/Supybot-0.83.4.1/
    python setup.py install
    popd
    mv /tmp/MeetBot-current/MeetBot ./ircbot/lib/python2.7/site-packages/supybot/plugins/MeetBot

## Configuring SupyBot

There's a wizard to do this, we have only a few answers to give but
some items need to go in the config file afterwards.

Launch `supybot-wizard` and fill in the blanks!

- Are you an advanced Supybot user? `no`
- Create default directories? `.`
- What IRC network will you be connecting to? `freenode`
- What server would you like to connect to? `leguin.freenode.net`
- Does this server require connection on a non-standard port? `n`

The nickname you pick next will also be used for log and config files:

- What nick would you like your bot to use? `bottled`
- Do you need a password for the IRC server? `n`
- Do you want your bot to join some channels when it connects? `y`
- Which channels? `#bottled #myotherchan`

## Setting up Plugins

From the last steps a bunch of plugin configuration has already been
done. But we need a little bit more, including setting up:

- `ChannelLogger` to support writing logs to disk
- `MeetBot` to help chair our meeting and handle minutes

    Beginning configuration for Admin...   Done!
    Beginning configuration for User...    Done!
    Beginning configuration for Channel... Done!
    Beginning configuration for Misc...    Done!
    Beginning configuration for Config...  Done!

- Would you like to look at plugins individually? `y`
- What plugin would you like to look at? `ChannelLogger`
- Would you like to load this plugin? `y`

    Beginning configuration for ChannelLogger... Done!

-  What plugin would you like to look at? `MeetBot`

- Would you like to load this plugin? `y

    Beginning configuration for MeetBot... Done!

- Would you like add another plugin? `n`

## Securing the Bot

- Would you like to add an owner user for your bot? `y`
- What should the owner's username be? `<your_irc_handle>`
- What should the owner's password be? `yadayadayada`
- Would you like to set the prefix char(s) for your bot? `y`
- What would you like your bot's prefix character(s) to be? `#`
    
        All done! Your new bot configuration is fqdnbot.conf. If you're
        running a *nix based OS, you can probably start your bot with the
        command line "supybot fqdnbot.conf". If you're not running a *nix
        or similar machine, you'll just have to start it like you start
        all your other Python scripts.

## Running the Bot

To start it, launch your python virtual env again as before, and just run
`supybot <your_bot_name.conf>`. Then jump on IRC and PM your bot. You'll
need to authorise yourself `user identify <admin> <passwd>` before going
further.

In the IRC channel you want to run a meeting, simply issue `#startmeeting`
and follow on from there. Note how the bot changes topics, and make
sure you use `#info`, `#action` and others liberally throughout the meeting.
If you `#link <some_url>` this will be used as the base URL when the final
logs and HTML output are generated.

Good luck!

## Closing Thoughts

It was a real pain to install this the first time around. All the information
is available but not in a single clean place. I almost completed a Node.js
replacement in less time. More on that another time!

[supybot]: http://supybook.fealdia.org/latest/
[meetbot]: http://meetbot.debian.net/Manual.html
[source]: http://sourceforge.net/projects/supybot/files/latest/download
[meetbot's source]: http://code.zgib.net/MeetBot/MeetBot-current.tar.gz
