---
title: "Bridging with PF on OpenBSD"
date: 2007-03-28
comments: true
categories: [openbsd, pf]
---
As part of tidying up "all those damn cables" and making it possible to
walk around the study, I've been replacing the long 10+m cables I've used
over recent years with shorter cross-over cables. Some of the newer NICs
support automatic detection of the cable type & you can freely use normal
cables instead of cross-over without issues. My soekris box unfortunately
doesn't support this with its 3 onboard NICs. Anyway, once I've got the
cross-overs in place, the 2 workhorses, sendai & continuity, are wired
directly into the soekris. Setting up a bridge was as easy as:

- reading the `brconfig` & `bridgename.if` man pages
- setting up `hostname.ath0` with the desired IP & subnet
- configuring `hostname.sis0` & `sis1` with "up"
- and creating a simple `bridgename.bridge0` linking the three interfaces,
and accepting only IP traffic across them

The next step, getting PF to send the traffic the right way, proved a little
more troublesome....
