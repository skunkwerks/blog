---
title: "Updating CouchDB Windows build"
date: 2011-12-27
comments: true
categories: [couchdb]
---
I've updated [glazier] to support Microsoft's SDK 7.1 and therefore Visual
Studio 2010 SP1 as well. This represents a significant upgrade of most
dependencies for CouchDB and Erlang/OTP, namely:

### Erlang
- bump wxWidgets 2.8.11 to 2.8.12
- revert OpenSSL 1.0.0e to 0.9.8r to align with OTP R15B release notes
- start using OTP/R15B from now on

### CouchDB
- bump curl to 7.23.1
- bump ICU to 4.6.1
- use Mozilla SpiderMonkey 1.8.5

Feel free to try these out from [snapshots].

[glazier]: https://github.com/dch/glazier
[experimental]: https://github.com/dch/glazier/tree/experimental/sdk_7.1_couchdb_1.1.1
[snapshots]: http://home.apache.org/~dch/snapshots/

