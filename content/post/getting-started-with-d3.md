---
title: "Getting Started with D3"
date: 2013-09-01
comments: true
categories: [reviews, d3]
---
This O'Reilly book is part of the "Getting Started" series, which I have to
say I don't really find valuable other than for a quick flick through a new
topic.

This is not a reference book that you'll come back to time & time
again, nor is it going to stand the test of time, as is often the case
for tech books. Much of the material is now covered in blog posts and existing
documentation on the improved D3js site, which wasn't so slick at the time
of publishing.

If you're absolutely just beginning with Javascript & visualisation, this is a
place to start, with small steps between examples, albeit on the light side
for content. It has since been superceded by the infinitely better
prepared [Interactive Data Visualization for the Web], also from the same
publisher, so I'd recommend most folk purchase that instead.

This review was brought to you by the O'Reilly [blogger programme].

[blogger programme]: http://oreilly.com/bloggers/?cmp=ex-orm-blgr-dave-cottlehuber
[Interactive Data Visualization for the Web]: http://shop.oreilly.com/product/0636920026938.do
