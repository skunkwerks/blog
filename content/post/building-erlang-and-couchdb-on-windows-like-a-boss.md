---
title: "Building Erlang and CouchDB on Windows like a Boss"
date: 2012-01-10
comments: true
categories:
---
For the last 2 years, Ericsson's Erlang binaries have been built with a
legacy Visual Studio 2005 compiler, and provided limited instructions
on building Windows Erlang from source. This has made building CouchDB from
source on Windows particularly challenging, as almost all dependencies
such as Mozilla SpiderMonkey and IBM's ICU no longer officially support
such an old compiler.

I developed [glazier] for two reasons, namely document the Erlang
build process on newer compilers with a scripted installation, and to
provide a reference build of CouchDB on Windows by including steps
to build the required dependencies. A fringe benefit of this process
is that I now have debug symbols (.pdb files) available for both Erlang
and CouchDB, and can make use of the Microsoft tools for performance
optimisation in future.

I've developed an AMI (virtual machine on Amazon's EC2 infastructure) that
encapsulates the entire build chain, and makes building CouchDB on Windows
as easy as launching it and logging in.

Here's how, assuming you've already got an EC2 account:

1. Contact me `@dch__` or `dch` on
[irc/#couchdb](irc://freenode.net/couchdb)
 to get access to the AMI. Note it's in EU West only.
2. Log into the AWS [console] for the EU West region, and view available
images - you should see something similar to `ami-99c7fded` listed.
3. Launch an [instance] - you have a choice between spot which is what I use
for temporary development work such as generating and testing builds, or
a normal instance where you can shut it down without losing your work.
If you're mainly compiling, then the `c1.medium` is a good deal - 2 fast
cores at your service. If you're running CouchDB, then pick `m2.large`
or greater, multiple cores so you get that Erlang SMP goodness. Make sure your
security group has at least 5984, 6984, HTTP and HTTPS, and 3389 (RDP) enabled.
4. Once your instance has started up, connect over RDP & log in. If you're
    on a Mac, use either [CoRD], or [RDC] Microsoft's Remote Desktop tool.
    RDC has the best full-screen mode, but CoRD supports things like changing
    passwords etc. Go figure.
5. Optionally set up dropbox, using `d:\dropbox*.exe`
6. From the Start Menu, launch `SDK 7.1 x86 release` to get a terminal.

        pushd d:\relax\couchdb
        git clean -fdx && git reset --hard
        git fetch
        git checkout -t origin/1.3.x
        shell
        # pick reqd erlang, I'm using R15B01 now
        cd /cygdrive/d/relax/couchdb
        ./bootstrap
        couchdb_config.sh
        couchdb_build.sh

And that's it. Do note that there's not a lot of error checking in the
`couchdb_*.sh` scripts so make sure you've checked the logs thoroughly.

I use `git clean -fdx && git reset --hard` to clean up after a build.

[glazier]: https://github.com/dch/glazier/
[console]: https://console.aws.amazon.com/ec2/home?region=eu-west-1#s=Images
[instance]: http://aws.amazon.com/ec2/instance-types/
[cord]: http://cord.sourceforge.net/
[rdc]: http://www.microsoft.com/en-us/download/details.aspx?id=18140
