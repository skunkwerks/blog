---
title: "coloured git console on windows"
date: 2010-09-07
comments: true
categories:
---
Tired of plain text output running git on Windows? Missing your unix
[coloured command-line fu](http://asimilatorul.com/index.php/2009/09/20/git-on-windows-with-msysgit/)
? add two more environment variables under cygwin:

        LESS=FSRX
        TERM=cygwin

