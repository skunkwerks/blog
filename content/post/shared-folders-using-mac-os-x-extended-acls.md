---
title: "Shared folders using Mac OS X extended ACLs"
date: 2010-07-31
comments: true
categories: [mac,apps]
---
You want to set up a shared folder on the same Mac, like iTunes or
Aperture, so your family can share the same files. Let's assume you
have a group called "staff"; which everybody is already in, and a
folder called "common". This is a mac, this should be simple, right?
well.. almost with a bit of terminal-fu:

    FOLDER=common
    GROUP=staff
    mkdir $FOLDER
    chown -R $USER $FOLDER
    chown -R :$GROUP $FOLDER
    chmod -R g+rw $FOLDER
    chmod -RN $FOLDER
    chmod -RI $FOLDER
    chmod -R +a '$GROUP allow list,add_file,search,delete,\
      add_subdirectory,delete_child,file_inherit,directory_inherit' \
      $FOLDER
    ls -lde $FOLDER

You can use the slightly more permissive `'$GROUP allow list,add_file,search, \
  delete,add_subdirectory,delete_child,readattr,writeattr,readextattr, \
  writeextattr,readsecurity,file_inherit,directory_inherit'`
which allows that group to change permissions and ACLs as well.

By explanation;

- `chown -R :staff` changes the standard unix group
- `chmod -RN` and `-RI` remove any inherited and initial ACLs; this is not
needed for new folders but if you are converting an existing folder, this helps
a lot to clean up crap
- `chmod -R +a`  this is the permissions-mojo being applied
- `ls -lde` displays the extended permissions for a folder
note that the ACLs must _not_ have spaces in them if you are pasting
code from above

Further reading:

- the [manual](http://developer.apple.com/mac/library/documentation/Darwin/Reference/ManPages/man1/chmod.1.html)
- [sage uni](http://sage.ucsc.edu/~wgscott/xtal/wiki/index.php/ACL:_Access_Control_Lists)
- [ars](http://arstechnica.com/apple/reviews/2005/04/macosx-10-4.ars/8) technica
- [ideo](http://www.ideocentric.com/matt/technology/osx-acl)
