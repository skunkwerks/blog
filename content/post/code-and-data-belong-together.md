---
title: "Code and Data belong together"
date: 2012-05-21
comments: true
categories: [couchdb, concepts, couchapp]
---

After a couple of years of using and abusing CouchDB, I think some of the
unique concepts embodied in Couch Apps needs to be spelled out.

1. Code and data belong together
1. Keep your data alive and breathing
1. Share widely

Let's look at the implications of this in more details:

## Code and Data belong together

Data on its own is largely useless to most of us. And ditto for code.
But bring the two together, and you have a powerful story to tell. By
distributing raw data (e.g. census figures, or geograhic data), and
your tools to transform and present that data together, others can mine
your data for different interpretations, or even merging and remixing it
with other sources and applications.

## Keep your Data Alive

One of CouchDB's most powerful features is the `_changes` feed, simply
an HTTP endpoint per database, optionally filtered, of all changes taking
place in the database. Users can connect and disconnect at will, allowing
offline models with occasional connectivity, or allowing customers or
other users to get up to date data at their convenience. The savings
from a simple bandwidth perspective are also significant, obviously
if your data is JSON-compatible to begin with.

## Share Widely

With the above two features, we're now in a position to develop and
evolve complex web-enabled applications, without being tied to an app
store, or a platform, nor connectivity. We can send users out into the
field, in medical situations with limited connectivity, and free them
from expensive phone calls, or even the need for a high-end central server.
Over time, when designed correctly, applications and data sets simply
converge, eventually consistent.

## Post Post

These are not the sorts of things people usually think of, with respect
to databases -- complex middleware, application-specific code, and
heavy dependencies between components would normally be required.

The applications and opportunities available with CouchDB are fundamentally
different to what's been possible in the past.
