---
title: "set up OpenBSD sendmail to fwd all local user mail to a smart host"
date: 2008-01-12
comments: true
categories: [openbsd, sendmail]
---
three changes required -

- configure sendmail to use a remote host for all mail in `/etc/mail/submit.cf`

        # changes to fwd mail directly to smart host
        #D{MTAHost}[127.0.0.1]
        D{MTAHost}[smtp.muse.net.nz]

- configure local aliases mapping to remap users to a destination address in
  `/etc/mail/aliases`
  
        # Well-known aliases -- these should be filled in!
        # root:
        root:           root@muse.net.nz

- permit relaying on smart host (postfix in my case) in `/etc/postfix/main.cf`

        mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain, akai.$mydomain
