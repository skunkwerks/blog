---
title: "running OpenAFS on OpenBSD"
date: 2007-11-24
comments: true
categories: [openbsd,openafs]
---

I've had trouble getting OpenAFS to run recently (since 1.4.5 I think) on
OpenBSD; I am still using my 4.0 binaries on 4.1 (I know, I know...) but
here's a few notes that may help you get started.

    cvs -d:pserver:anonymous@cvs.openafs.org:/cvs login anonymous
    cvs -z9 -d:pserver:anonymous@cvs.openafs.org:/cvs co -PA openafs# update
    cd /usr/ports/local/openafs && cvs -Rvz9 update -Pd
    chgrp -R wsrc .
    find . -type d | xargs chmod 775
    find . -type f | xargs chmod 664

    ./configure --enable-shared --enable-fast-restart --enable-bitmap-later \
     --quiet --enable-debug --enable-bos-new-config --enable-supergroups \
     --enable-namei-fileserver --enable-largefile-fileserver \
     --disable-kernel-module --with-afs-sysname=i386_obsd40 \
     --enable-transarc-paths

    make && make install DESTDIR=/tmp/openafs-1.4.4
    cd /tmp/openafs-1.4.4 && tar cvzf /usr/ports/local/openafs-1.4.4_obsd40.tar.gz .
    make && make install DESTDIR=/tmp/openafs-1.5.15

    cd /tmp/openafs-1.5.15 && tar cvzf /usr/ports/local/openafs-1.5.15_obsd40.tar.gz .
    rm -rf /usr/local/lib/afs/ /usr/local/libexec/openafs/ /usr/local/include/rx/ /usr/local/include/afs/
    rm /usr/local/include/{des.h,des_conf.h,des_odd.h,des_prototypes.h,lock.h,lwp.h,mit-cpyright.h,potpourri.h,preempt.h}
    rm /usr/local/include/{timer.h,ubik.h,ubik_int.h}
    rm /usr/local/lib/{libafsauthent.a,libafsrpc.a,libdes.a,liblwp.a,librx.a,librxkad.a,librxstat.a,libubik.a}
    rm /usr/local/bin/{afsmonitor,bos,cmdebug,compile_et,dlog,dpass,fs,klog,klog.krb,knfs,kpasswd,kpwvalid,livesys,pagsh,pagsh.krb,pts,rxgen,scout,sys,tokens,tokens.krb,translate_et,udebug,unlog,up,xstat_cm_test,xstat_fs_test}
    rm /usr/local/sbin/{afsd,backup,bos_util,bosserver,butc,copyauth,fms,fstrace,kadb_check,kas,kdb,kdump/kdump-build,kpwvalid,kseal,prdb_check,pt_util,read_tape,restorevol,rmtsysd,rxdebug,uss,vldb_check,vldb_convert,voldump,volinfo,vos,vsys}

## Setting up the OpenAFS client

- install openafs-1.*.tgz package
- setup krb5.conf as usual
- set up afs.conf as usual

`/etc/rc.securelevel`

    /sbin/modload /usr/local/lib/openafs/libafs.o

`/etc/rc.local`

    # start OpenAFSD client
    if [ ';X${openafsd}'; != X';NO'; ]; then
     echo -n 'starting OpenAFS client...';
     mkdir -p -m 0755 /afs
     mkdir -p /var/openafs/cache
     /usr/local/sbin/afsd -stat 4000 -dcache 4000 -daemons 6 -volumes 256 -files 50000 \
             -afsdb -fakestat -nosettime
     echo 'done!'
    fi

`/etc/rc.conf.local`

    #start OpenAFS client
    openafsd = YES
    file:/etc/openafs/cacheinfo
    /afs:/var/openafs/cache:1048576
    # set up for next reboot

`/etc/rc.conf.local`

    # start ARLA client
    # add '--dynroot' on non-AFSDB servers
    echo afs=YES>> /etc/rc.conf.local
    echo afsd_flags=\';--log=/var/log/arlad.log --cpu-usage --check-consistency\';>> /etc/rc.conf.local
    # edit ntpd.conf for time.muse.net.nz
    # install client configuration
    cd /tmp && ftp ftp://gremlin.muse.net.nz/OpenAFS/openafs_obsd39.tar.gz
    cd / && tar xvzpf /tmp/openafs_obsd39.tar.gz
    mkdir -p -m 0755 /afs
    echo -n 'unmounting afs:'
    pkill afsd > /dev/null 2>&amp;1
    rm -rf /var/spool/afs
    umount /afs
    rm /var/log/arlad.log
    pgrep afsd && echo FAIL unable to shut down existing instance

    echo -n 'mounting afs:'
    mount -t xfs /dev/xfs0 /afs
    /usr/libexec/afsd --log=/var/log/arlad.log --recover --cpu-usage --check-consistency
    ls /afs

### tweak `/etc/rc*`

A number of changes are required on the OpenAFS server in various `/etc/rc*` files:

`/etc/rc.local`

    # start OpenAFSD server
    if [ ';X${openafs}'; = X';YES'; ]; then
     echo -n 'starting OpenAFS daemons...';
     /usr/afs/bin/bosserver ${openafsd_flags}
     echo 'done!'
    fi

`/etc/rc.conf.local`

    # OpenAFS server
    openafs=YES
    openafsd_flags=';-log -auditlog /var/log/boss.log -syslog -enable_peer_stats -enable_process_stats';

`/etc/rc.shutdown`

    echo -n 'stopping OpenAFS daemons...';
    /usr/afs/bin/bos shutdown localhost -localauth -wait
    /usr/afs/bin/bos status localhost -localauth
    echo 'done!'

### start up OpenAFS

    /usr/afs/bin/bosserver -log -auditlog /var/log/boss.log -syslog \
     -enable_peer_stats -enable_process_stats

## Enabling AFSDB in DNS:

`/var/named/master/muse.net.nz`

    sendai                  A       10.0.0.x                 ; afsdb1
    muse.net.nz        .    3600    IN AFSDB 1      sendai

Validate config:

    dig -t AFSDB muse.net.nz
    ;; ANSWER SECTION:
    muse.net.nz.            3600    IN      AFSDB   1 straylight.muse.net.nz.
    muse.net.nz.            3600    IN      AFSDB   1 sendai.muse.net.nz.
    muse.net.nz.            3600    IN      AFSDB   1 wintermute.muse.net.nz.
    
    ;; ADDITIONAL SECTION:
    straylight.muse.net.nz  259200  IN      A       10.0.0.22
    sendai.muse.net.nz      259200  IN      A       10.0.0.20
    wintermute.muse.net.nz  259200  IN      A       10.0.0.21

## Create AFS Service Accounts

See [dementia](http://www.dementia.org/twiki/bin/view/AFSLore/KerberosAFSInstall)
for further information.

    kadmin -p admin/krb
    kadmin> add --random-key --use-defaults afs/muse.net.nz
    kadmin> del_enctype afs/muse.net.nz des3-cbc-sha1
    kadmin> del_enctype afs/muse.net.nz aes256-cts-hmac-sha1-96
    kadmin> del_enctype afs/muse.net.nz arcfour-hmac-md5
    kadmin> list *afs*
     admin/afs
     afs/muse.net.nz
     host/afsdb.muse.net.nz
    kadmin> get afs/muse.net.nz@MUSE.NET.NZ
                Principal: afs/muse.net.nz@MUSE.NET.NZ
        Principal expires: never
         Password expires: never
     Last password change: never
          Max ticket life: 1 day
       Max renewable life: 1 week
                     Kvno: 1
                    Mkvno: 0
    Last successful login: never
        Last failed login: never
       Failed login count: 0
            Last modified: 2007-03-12 04:28:42 UTC
                 Modifier: kadmin/admin@MUSE.NET.NZ
               Attributes:
                 Keytypes: des-cbc-md5(pw-salt), des-cbc-md4(pw-salt), des-cbc-crc(pw-salt)
    kadmin> ext -k /etc/afskeytabfile.krb5 afs/muse.net.nz
    kadmin> quit

## Check keytab

    root@ice:/ $ ktutil -k /etc/afskeytabfile.krb5 list

`/etc/afskeytabfile.krb5`

    Vno  Type         Principal
       1  des-cbc-md5  afs/muse.net.nz@MUSE.NET.NZ
       1  des-cbc-md4  afs/muse.net.nz@MUSE.NET.NZ
       1  des-cbc-crc  afs/muse.net.nz@MUSE.NET.NZ

## Wire up AFS client

    root@ice:/ $ mkdir -p /usr/afs/etc
    root@ice:/ $ ln -s /etc/afs/ThisCell /usr/afs/etc/ThisCell
    
    root@ice:/ $ cat /etc/afs/ThisCell
    muse.net.nz
    
    root@ice:/ $ ktutil copy FILE:/etc/afskeytabfile.krb5 AFSKEYFILE:/usr/afs/etc/KeyFile
    
    ## may need to mkdir -p /usr/afs/etc/;
    ## ln -s /etc/afs/ThisCell /usr/afs/etc/ThisCell
    ## mkdir -m 700 p /etc/openafs/server
    
    ## oldversion: ktutil -v copy /tmp/afsv5key AFSKEYFILE:/etc/openafs/server/KeyFile
    ## ktutil -v copy /tmp/afsv5key AFSKEYFILE:/etc/openafs/server/KeyFile
    /usr/afs/bin/bosserver -syslog -noauth
    /usr/afs/bin/bos listkeys -noauth afsdb.muse.net.nz
    /usr/afs/bin/bos setcellname afsdb.muse.net.nz muse.net.nz -noauth
    #chmod this appropriately!

The next set of tips are basically harvested and tested all from:

- http://www.openafs.org/pages/doc/QuickStartUnix/auqbg005.htm#HDRWQ50
- http://www.arayan.com/da/yazi/OpenAFS_Kerberos_5.html
- http://wiki.bsd-crew.de/index.php/OpenAFS_unter_NetBSD#Compiling_OpenAFS
- http://www.pdc.kth.se/heimdal/heimdal.html#Testing-clients-and-servers
- http://www.slac.stanford.edu/~alfw/kerb5.html
- http://grand.central.org/twiki/bin/view/AFSLore/InstallingtheFirstAFSMachine

### Set up client

    mkdir -p -m 700 /usr/afs/db
    mkdir -p m 755  /var/openafs/{local,server,cache,logs}
    /bin/echo ';/afs:/var/openafs/cache:198112'; > /etc/openafs/cacheinfo
    /bin/echo muse.net.nz> /etc/afs/ThisCell
    /bin/echo ';>muse.net.nz            #where great ideas come together';>> /etc/afs/CellServDB
    /bin/echo ';10.0.0.32    #afsdb.muse.net.nz';>> /etc/afs/CellServDB
    mkdir -p /usr/vice/etc
    mkdir -p /usr/afs/etc
    scp afsdb.muse.net.nz:/etc/openafs/server/KeyFile /etc/afs/
    ln -s /etc/afs/KeyFile /usr/afs/etc/KeyFile
    ln -s /etc/afs/ThisCell /usr/vice/etc/ThisCell
    ln -s /etc/afs/CellServDB /usr/vice/etc/CellServDB
    ln -s /etc/afs/CellServDB /usr/afs/etc/CellServDB
    ln -s /etc/afs/ThisCell /usr/afs/etc/ThisCell
    ln -s /etc/afs/CellServDB /etc/openafs/CellServDB
    ln -s /etc/afs/ThisCell /etc/openafs/ThisCell
    ln -s /etc/afs/CellServDB /etc/openafs/server/CellServDB
    ln -s /etc/afs/ThisCell /etc/openafs/server/ThisCell
    ln -s /etc/openafs/server/KeyFile /etc/openafs/KeyFile
    mv /usr/sbin/fs /usr/sbin/fs.arla
    mv /usr/sbin/bos /usr/sbin/bos.arla
    mv /usr/sbin/vos /usr/sbin/vos.arla
    mv /usr/sbin/pts /usr/sbin/pts.arla
    mkdir -p /usr/afs/local
    echo 10.0.0.32>/usr/afs/local/NetInfo
    echo 127.0.0.1>/usr/afs/local/NetRestrict
    echo 10.0.0.3>>/usr/afs/local/NetRestrict
    echo 10.0.0.12>>/usr/afs/local/NetRestrict
    echo 10.0.0.20>>/usr/afs/local/NetRestrict
    echo 10.0.0.25>>/usr/afs/local/NetRestrict
    echo 10.0.0.27>>/usr/afs/local/NetRestrict
    echo admin.afs>/usr/afs/etc/UserList

### When vice isn't a separate volume

If you're not using a separate mount point for vicepXX

    touch /vicepa/AlwaysAttach

## Set up daemons

    alias pafs='ps aux | grep afs'
    /usr/afs/bin/bosserver -log -syslog -noauth
    /usr/afs/bin/bos setcellname afsdb.muse.net.nz muse.net.nz -noauth
    /usr/afs/bin/bos adduser afsdb.muse.net.nz admin.afs -noauth
    more /etc/afs/ThisCell
    more /etc/afs/CellServDB
    >muse.net.nz    #Cell name
    [10.0.0.32]        #afsdb.muse.net.nz
    /usr/afs/bin/bos listhosts afsdb.muse.net.nz -noauth
    /usr/afs/bin/bos create afsdb.muse.net.nz \
     buserver simple /usr/afs/bin/buserver \
     -cell muse.net.nz -noauth
    /usr/afs/bin/bos create afsdb.muse.net.nz \
     ptserver simple /usr/afs/bin/ptserver  \
     -cell muse.net.nz -noauth
    /usr/afs/bin/bos create afsdb.muse.net.nz \
     vlserver simple /usr/afs/bin/vlserver  \
     -cell muse.net.nz -noauth
    pafs

### Grant krb admins afs rights
    
    /usr/afs/bin/pts createuser -name admin.afs -cell muse.net.nz -noauth
    /usr/afs/bin/pts adduser admin.afs system:administrators -cell muse.net.nz -noauth
    /usr/afs/bin/pts createuser -name dave -cell muse.net.nz -noauth
    /usr/afs/bin/pts createuser -name pk -cell muse.net.nz -noauth
    /usr/afs/bin/pts createuser -name veronika -cell muse.net.nz -noauth
    ## /usr/afs/bin/pts adduser dave system:administrators -cell muse.net.nz -noauth
    /usr/afs/bin/pts mem system:administrators -cell muse.net.nz -noauth
    /usr/afs/bin/bos listkeys afsdb.muse.net.nz -cell muse.net.nz -noauth

### restart BOS with authentication

    /usr/afs/bin/bos shutdown afsdb.muse.net.nz -cell muse.net.nz -localauth -wait
    /usr/afs/bin/bos status afsdb.muse.net.nz -cell muse.net.nz -localauth -long
    pafs
    pkill -HUP bosserver
    pafs
    ### mount /vicepa
    ### kinit admin/afs to get afs privileged tokens &amp; make sure you have arla running
    /usr/afs/bin/bosserver -log -syslog -enable_peer_stats -enable_process_stats
    /usr/afs/bin/bos restart afsdb.muse.net.nz -all -cell muse.net.nz

### create FS instance

    /usr/afs/bin/bos create afsdb.muse.net.nz fs fs \
     /usr/afs/bin/fileserver \
     /usr/afs/bin/volserver \
     /usr/afs/bin/salvager \
     -cell muse.net.nz -localauth

### create AFS root volume

    /usr/afs/bin/vos listpart afsdb.muse.net.nz
    /usr/afs/bin/vos create afsdb.muse.net.nz /vicepa root.afs \
     -cell muse.net.nz -verbose
    /usr/afs/bin/vos create afsdb.muse.net.nz /vicepa root.cell \
     -cell muse.net.nz -verbose
    /usr/afs/bin/vos listvol -server afsdb.muse.net.nz

### Set up the cell root volumes

    #fix up ln -s stuff
    #identify correct paths for bins
    #move KeyFile to continuity
    fs setacl /afs system:administrators rlidwka
    fs setacl /afs system:anyuser rl
    fs mkmount /afs/muse.net.nz root.cell
    dir /afs/muse.net.nz
    fs setacl /afs/muse.net.nz system:administrators rlidwka
    fs setacl /afs/muse.net.nz system:anyuser rl
    fs mkmount /afs/.muse.net.nz root.cell -rw
    dir /afs/.muse.net.nz
    fs setacl /afs/.muse.net.nz system:administrators rlidwka
    fs setacl /afs/.muse.net.nz system:anyuser rl
    fs mkmount -dir /afs/su.se -vol root.cell -cell su.se -fast
    fs mkmount -dir /afs/openafs.org -vol root.cell -cell openafs.org -fast
    fs mkmount -dir /afs/stacken.kth.se -vol root.cell -cell stacken.kth.se -fast
    /usr/afs/bin/pts removeuser dave system:administrators -cell muse.net.nz

replicate root volumes and increase their quot

    /usr/afs/bin/vos addsite afs1.muse.net.nz /vicepa root.cell
    /usr/afs/bin/vos addsite afs1.muse.net.nz /vicepa root.afs
    /usr/afs/bin/vos addsite afs1.muse.net.nz /vicepa root.home
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz
    /usr/afs/bin/vos release root.afs -verbose
    /usr/afs/bin/vos release root.cell -verbose
    /usr/afs/bin/vos release root.home -verbose
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz
    /usr/afs/bin/vos examine root.cell -format
    /usr/afs/bin/vos examine root.afs -extended
    /usr/afs/bin/vos setfields root.cell -maxquota 100000
    /usr/afs/bin/vos setfields root.afs -maxquota 10000
    /usr/afs/bin/vos examine root.cell
    /usr/afs/bin/vos release root.cell -verbose
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz

create basic structure

    /usr/afs/bin/vos create afs.muse.net.nz /vicepa root.home -verbose
    fs setacl /afs/.muse.net.nz system:authuser rl
    fs mkmount /afs/.muse.net.nz/home root.home -rw
    /usr/afs/bin/vos release root.cell -verbose
    ll  /afs/.muse.net.nz/home/dave
    fs setacl /afs/.muse.net.nz/home system:authuser rl
    fs setacl /afs/.muse.net.nz/home system:administrators rlidwka
    vos examine root.home -format
    /usr/afs/bin/vos release root.cell -verbose

create a single person

    /usr/afs/bin/vos listpart afs.muse.net.nz
    /usr/afs/bin/vos create afs.muse.net.nz /vicepa home.dave -verbose
    fs mkmount /afs/.muse.net.nz/home/pk home.pk -rw
    /usr/afs/bin/vos listvol -server finn
    /usr/afs/bin/vos examine home.pk -format
    /usr/afs/bin/vos setfields home.pk -maxquota 500000
    /usr/afs/bin/vos release root.home
    ll  /afs/.muse.net.nz/home/pk
    /usr/afs/bin/pts createuser pk
    fs setacl /afs/.muse.net.nz/home/pk system:administrators rlidwka
    fs setacl /afs/.muse.net.nz/home/pk pk write
    /usr/afs/bin/vos release root.cell -verbose
    dir /afs/muse.net.nz/home/dave

add your new FS & IP to DNS as AFSDB

## Duplicate to new fileserver

      /usr/afs/etc
     bin:        /usr/afs/bin
     bin:        /usr/vice/etc/libafs.o
      /usr/vice/etc
      /etc/afs
      /etc/kerberosV except krb5.keytab

#### Create KeyTab

create a new `krb5.keytab` for this host in `/etc/kerberosV/krb5.keytab`

    sudo -s
    kadmin -p dave/admin
     add --random-key host/continuity.muse.net.nz
     ext --keytab=/etc/kerberosV/krb5.keytab host/continuity.muse.net.nz
     exit
    ktutil  -k /etc/kerberosV/krb5.keytab list
    chmod 0400 /etc/kerberosV/krb5.keytab
    /usr/afs/bin/bosserver -log -syslog -enable_peer_stats -enable_process_stats
    /usr/afs/bin/bos listhosts afs1.muse.net.nz -localauth
    echo 10.0.0.3>/usr/afs/local/NetInfo

grant admin rights on local fileserver if not done above

    /usr/afs/bin/bos adduser afs1.muse.net.nz dave.afs -localauth
    /usr/afs/bin/bos listkeys afs1.muse.net.nz -localauth
    kinit --afslog admin/afs
    /usr/afs/bin/bos restart afs1.muse.net.nz -all
    /usr/afs/bin/bos create afs1.muse.net.nz fs fs \
     /usr/afs/bin/fileserver \
     /usr/afs/bin/volserver \
     /usr/afs/bin/salvager \
     -cell muse.net.nz
    /usr/afs/bin/vos listpart afs1.muse.net.nz
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz

add RO replicas

    /usr/afs/bin/vos addsite afs1.muse.net.nz /vicepa root.cell
    /usr/afs/bin/vos addsite afs1.muse.net.nz /vicepa root.afs
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz
    /usr/afs/bin/vos release -verbose root.cell
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz
    /usr/afs/bin/vos release -verbose root.afs
    /usr/afs/bin/vos listvol -server afs1.muse.net.nz
    
    /usr/afs/bin/vos create finn /vicepa root.public -verbose
    /usr/afs/bin/vos addsite finn /vicepa root.public -verbose
    /usr/afs/bin/vos release root.public
    /usr/afs/bin/vos listvol -server finn
    fs mkmount /afs/.muse.net.nz/pub root.public -rw
    /usr/afs/bin/vos release root.cell --verbose
    ll /afs/.muse.net.nz/pub
    fs setacl /afs/.muse.net.nz/pub system:anyuser rl
    dir /afs/muse.net.nz/pub
    /usr/afs/bin/vos create finn /vicepa public.openbsd37 -verbose
    /usr/afs/bin/vos setfields public.openbsd37  -maxquota 1000000
    /usr/afs/bin/vos addsite finn /vicepa public.openbsd37 -verbose
    /usr/afs/bin/vos release public.openbsd37
    /usr/afs/bin/vos listvol -server finn
    mkdir /afs/.muse.net.nz/pub/OpenBSD/
    fs listacl /afs/.muse.net.nz/pub/OpenBSD
    fs mkmount /afs/.muse.net.nz/pub/OpenBSD/3.7 public.openbsd37 -rw
    fs listacl /afs/.muse.net.nz/pub/OpenBSD/3.7
    fs setacl /afs/.muse.net.nz/pub/OpenBSD/3.7 system:anyuser rl
    /usr/afs/bin/vos release root.public
    dir /afs/muse.net.nz/public
    /usr/afs/bin/vos addsite wintermute /vicepa root.public -verbose
    /usr/afs/bin/vos addsite wintermute /vicepa public.openbsd37 -verbose
    /usr/afs/bin/vos release root.public
    /usr/afs/bin/vos release public.openbsd37
    /usr/afs/bin/vos listvol -server wintermute
    
    /usr/afs/bin/vos create finn /vicepa public.distfiles -verbose
    fs mkmount /afs/.muse.net.nz/pub/distfiles public.distfiles -rw
    fs sa /afs/.muse.net.nz/pub/distfiles system:anyuser rl
    fs sa /afs/.muse.net.nz/pub/distfiles dave write
    
    /usr/afs/bin/vos listpart wintermute.muse.net.nz
    /usr/afs/bin/vos create afs.muse.net.nz /vicepa home.pk -verbose
    fs mkmount /afs/.muse.net.nz/home/pk home.pk -rw
    /usr/afs/bin/vos listvol -server wintermute
    /usr/afs/bin/vos setfields home.pk -maxquota 500000
    /usr/afs/bin/pts createuser -name pk
    fs setacl /afs/.muse.net.nz/home/pk system:administrators rlidwka
    fs setacl /afs/.muse.net.nz/home/pk pk write
    /usr/afs/bin/vos release root.home
    dir /afs/muse.net.nz/home/pk
    
    /usr/afs/bin/pts creategroup system:servers system:administrators
    /usr/afs/bin/pts creategroup system:wsrc system:administrators
    /usr/afs/bin/pts listent -g
    /usr/afs/bin/pts listent -u
    /usr/afs/bin/pts createuser 10.0.0.9
    /usr/afs/bin/pts createuser 10.0.0.2
    /usr/afs/bin/pts add 10.0.0.9 system:servers
    /usr/afs/bin/pts add 10.0.0.2 system:servers
    /usr/afs/bin/pts mem system:servers
    /usr/afs/bin/pts add dave system:wsrc
    /usr/afs/bin/pts mem system:wsrc

example of setting up wsrc

    fs la /afs/.muse.net.nz/i386_obsd37
     Access list for /afs/.muse.net.nz/i386_obsd37 is
     Normal rights:
       system:wsrc rlidwk
       system:servers rl
       system:administrators rlidwka
       system:authuser rl

A useful debugging tip is to run boss in the foreground,
 `bosserver -log -enable_peer_stats -enable_process_stats -nofork`

`/usr/afs/etc/CellServDB`

    >muse.net.nz                # dave & veronika
    >ualberta.ca                # University of Alberta
    >stacken.kth.se             # Royal Institute of Technology Computer Club
    >su.se                      # Stockholm University
    >openafs.org                #

`/usr/afs/etc/ThisCell`

    muse.net.nz

`/usr/afs/etc/SuidCells`

    # n/a

`/usr/afs/etc/CellAlias`

    muse.net.nz        muse

`/usr/afs/etc/afsd.conf`

    high_vnodes        12000
    low_vnodes          9000
    high_bytes          6000M
    low_bytes           1000M
    numcreds             100
    numconns             100
    numvols              100
    fetch_block            4M

`/usr/vice/etc/`

    CellServDB -> /usr/afs/etc/CellServDB
    KeyFile -> /usr/afs/etc/KeyFile
    ThisCell -> /usr/afs/etc/ThisCell
    libafs.o

`/etc/afs/`

    CellServDB -> /usr/afs/etc/CellServDB
    SuidCells -> /usr/afs/etc/SuidCells
    ThisCell -> /usr/afs/etc/ThisCell
    afsd.conf -> /usr/afs/etc/afsd.conf

`/etc/sysctl.conf`

  ddb.panic=0                    # 0=Do not drop into ddb on a kernel panic
  kern.maxfiles=102400           # increase maximum files

`/etc/login.conf`

    daemon:\
         :openfiles-cur=1280:\

    tar cvzf /tmp/openafs.muse.cfg.tar.gz /etc/openafs /etc/kerberosV/krb5.conf /var/openafs /etc/afs
    KAS=/usr/local/sbin/kas
    BOS=/usr/afs/bin/bos
    FS=/usr/local/bin/fs
    VOS=/usr/afs/bin/vos
    PTS=/usr/afs/bin/pts
    mkdir -m 700 /var/openafs /usr/afs/db
    mkdir -m 755  /etc/openafs /etc/openafs/server /var/openafs/{local,server,cache}
    /bin/echo ';/afs:/var/openafs/cache:198112'; > /etc/openafs/cacheinfo
    /bin/echo muse.net.nz> /etc/afs/ThisCell
    /bin/echo ';>muse.net.nz            #where great ideas come together';>> /etc/afs/CellServDB
    ln -s /etc/afs/CellServDB /usr/afs/etc/CellServDB
    ln -s /etc/afs/ThisCell /usr/afs/etc/ThisCell
    ln -s /etc/afs/CellServDB /usr/afs/CellServDB
    ln -s /etc/afs/ThisCell /usr/vice/etc/ThisCell
    ln -s /etc/openafs/server/KeyFile /etc/openafs/KeyFile
    mv /usr/sbin/fs /usr/sbin/fs.arla
    mv /usr/sbin/bos /usr/sbin/bos.arla
    mv /usr/sbin/vos /usr/sbin/vos.arla
    mv /usr/sbin/pts /usr/sbin/pts.arla
    echo 10.0.0.8>/usr/afs/local/NetInfo
    echo 127.0.0.1>/usr/afs/local/NetRestrict
    scp continuity:/etc/openafs/server/KeyFile /etc/openafs/server/
    /usr/afs/bin/bosserver
    pgrep boss

### grant admin rights on local fileserver if not done above

    /usr/afs/bin/bos adduser afs.muse.net.nz dave.afs -localauth
    /usr/afs/bin/bos listkeys afs.muse.net.nz -localauth
    /usr/afs/bin/bos restart afs.muse.net.nz -all -localauth
    /usr/afs/bin/bos create afs.muse.net.nz fs fs \
     /usr/afs/bin/fileserver \
     /usr/afs/bin/volserver \
     /usr/afs/bin/salvager \
     -localauth
    /usr/afs/bin/vos listpart afs.muse.net.nz
    /usr/afs/bin/vos listvol -server afs.muse.net.nz
    /usr/afs/bin/bos shutdown afs.muse.net.nz -localauth
    /usr/afs/bin/bos restart afs.muse.net.nz -all
