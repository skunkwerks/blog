---
title: "Slurping data back out of CouchDB"
date: 2011-12-22
comments: true
categories:
---
While this example is slightly contrived, we can re-use CouchDB to
extract this data again.

There are several ways, so let's try each of them.

First, and easiest, is to request a list of all documents in the
DB, and then use `xargs wget` to pull the data down.

    $ DB=http://localhost:5984/posts
    $ curl --silent $DB/_all_docs |
        sed -nE 's/{"id":"([0-9]+)".*/\1/p'
    21298063
    21298065
    ...
    31797293
    66018817
    $
