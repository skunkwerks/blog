---
title: "using KerberosV on OpenBSD"
date: 2007-11-24
comments: true
categories: [openbsd, kerberos]
---
setting up and using Kerberos V on OpenBSD is a piece of cake. With this
info, you should also be able to get k-enabled OpenAFS and ssh working too.

references:

- [OpenBSD] FAQ
- [Heimdal] reference
- Running [AFS on NetBSD]
- The AFS [install wiki]

[OpenBSD]: http://www.openbsd.org/faq/faq10.html#Kerberos
[Heimdal]: http://www.pdc.kth.se/heimdal/heimdal.html
[AFS on NetBSD]: http://kula.public.iastate.edu/talks/afs-bpw-2005/afs-bpw-2005-iowa.html
[install wiki]: http://wiki.openafs.org/GettingStarted/

`/etc/kerberosV/krb5.conf`

    [libdefaults]
      default_realm = MUSE.NET.NZ
      ticket_lifetime = 6000
      clockskew = 300
    
    [appdefaults]
      afs-use-524 = no
      afslog = yes
    
    [realms]
      MUSE.NET.NZ = {
        supported_keytypes = des:normal des-cbc-crc:v4 des-cbc-crc:afs3
        kdc = kerberos.muse.net.nz
        admin_server = kerberos.muse.net.nz
        kpasswd_server = kerberos.muse.net.nz
      }

    [domain_realm]
      .muse.net.nz = MUSE.NET.NZ

    [kadmin]
      default_keys = v5 afs3
      afs-cell = muse.net.nz

    [logging]
      kadmind = FILE:/var/heimdal/kadmind.log

    [kdc]
      require-preauth = no
      v4-realm = MUSE.NET.NZ
      afs-cell = muse.net.nz

# Setting up Heimdal
    
    mkdir /var/heimdal
    chmod 700 /var/heimdal
    cd /var/heimdal
    kstash
    # enter your password

# Create the default kerberos security principals using `kadmin -l`
    
    init MUSE.NET.NZ
    add --use-defaults admin/krb
    add --use-defaults admin/afs
    add --use-defaults dave
    add --use-defaults veronika
    add --use-defaults pk
    add --random-key --use-defaults host/kerberos.muse.net.nz
    add --random-key --use-defaults host/wintermute.muse.net.nz
    add --random-key --use-defaults host/sendai.muse.net.nz
    add --random-key --use-defaults host/straylight.muse.net.nz
    add --random-key --use-defaults host/continuity.muse.net.nz
    add --random-key --use-defaults host/finn.muse.net.nz
    ext --keytab=/etc/kerberosV/krb5.keytab host/finn.muse.net.nz
    ext --keytab=/etc/kerberosV/krb5.keytab host/continuity.muse.net.nz
    ext --keytab=/etc/kerberosV/krb5.keytab host/kerberos.muse.net.nz
    ext --keytab=/etc/kerberosV/krb5.keytab host/sendai.muse.net.nz
    ext --keytab=/etc/kerberosV/krb5.keytab host/straylight.muse.net.nz
    exit
    
    chmod 0400 /etc/kerberosV/krb5.keytab
    cd /tmp
    sudo nohup /usr/libexec/kdc &amp;
    sudo nohup /usr/libexec/kadmind &amp;
    sudo nohup /usr/libexec/kpasswdd &amp;


`/var/heimdal/kadmind.acl`

        admin/krb@MUSE.NET.NZ    all
        muffin/admin@MUSE.NET.NZ all              */
        joe/admin@MUSE.NET.NZ    all              *@MUSE.NET.NZ
        jim/admin@MUSE.NET.NZ    all            */*@MUSE.NET.NZ
        jon/admin@MUSE.NET.NZ    change-password  *@MUSE.NET.NZ

And for each host to be member of kerberos realm:

- use same `/etc/kerberosV/krb5.conf` as before

        sudo kadmin -p admin/krb@MUSE.NET.NZ
        add --random-key  --use-defaults host/afsdb.muse.net.nz
        ext --keytab=/etc/kerberosV/krb5.keytab host/afsdb.muse.net.nz
        chmod 0400 /etc/kerberosV/krb5.keytab

# Using Kerberised ssh


`/etc/ssh/sshd_config`

    # Kerberos options
    #KerberosAuthentication no
    KerberosAuthentication yes
    #KerberosOrLocalPasswd yes
    #KerberosTicketCleanup yes
    #KerberosGetAFSToken no
    KerberosGetAFSToken yes
    
    # GSSAPI options
    #GSSAPIAuthentication no
    GSSAPIAuthentication yes
    #GSSAPICleanupCredentials yes

`/etc/ssh/ssh_config`

    Host *
    GSSAPIAuthentication yes
    GSSAPIDelegateCredentials yes

Test it all works:

    kinit -fp --afslog dave
    klist -vT
    
    # disable your .ssh/ for a moment
    ssh -v kerberos.muse.net.nz
    debug1: Authentications that can continue: publickey,gssapi-with-mic,password,keyboard-interactive
    debug1: Next authentication method: gssapi-with-mic
    debug1: Delegating credentials
    debug1: Delegating credentials
    debug1: Authentication succeeded (gssapi-with-mic).
