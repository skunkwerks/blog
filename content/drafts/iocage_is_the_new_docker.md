---
categories: [tech, freebsd]
title: "ioCage is the new Docker"
keywords: [zfs, freebsd, vagrant]
draft: true
date: 2015-08-06T22:56:02+02:00
---
i love things with terminals...
they are so cool

ioCage is a tool to manage containers efficiently and easily, on FreeBSD.
In my usage, it's proven to be significantly easier to use, robust, and
mature, in a way that Docker & linux containers simply are not, at least,
for the present moment.

This article will give you an overview of ioCage & it's supporting
technologies, and some hands-on examples to work with.

## Jails

[Jails] have been a part of FreeBSD for an exceptionally long time, and
every release brings more jailed goodies. FreeBSD 10.2 is very close to
release, and it's not surprisingly, already awesome!

The key feature of a FreeBSD Jail is partitioning a larger host into smaller
chunks that are presented back as fully operational hosts to a user, without
compromising security nor performance. Processes running in a jail suffer
no penalties for doing so. A full network stack and normal tools are
available if required, or permitted, by an administrator, and FreeBSD is a
common choice for hosting companies as a result. The handbook has great
[jail docs] of course, including an [architecture] section.

One of the other amazing features in FreeBSD is [ZFS], & it is the only
filesystem you should be running if your data has any value. Google up on
[bitrot] for more details, or Data Rot for the pedantic amongst you. It is
real, and will destroy your data without you knowing.

In general, [ZFS] allows us to manage directory trees as a single unit,
called datasets, independently of the physical media. These datasets
are very portable, have extrememly good performance and tuning options, and
can be compressed in realtime (highly recommended), archivable (standard
wire format), transferred directly over ssh or similar secured pipelines,
can be cloned, snapshotted, mirrored, moved around, mounted and unmounted,
and much much more. Underlying this logical flexibility is a filesystem
with unparalleled expandability and reliability designed in, with over a
decade of solid production at Sun Microsytems and other organisations.
Read iXsystems' site for a good [intro].

It is also possible to store an entire virtual machine in ZFS. I have been
doing this for a while using [VMWare Fusion] and [OpenZFS on OSX], and using
zfs snapshots to roll forwards and back, repeating tests and builds easily,
or cloning the dataset to use thin provisioned copies while running many
concurrent identical VMs.

There's a great tool called [iocage], that takes ZFS and Jails to an even
better place. iocage provides a simple and powerful tool chain to launch,
maintain, and upgrade, FreeBSD Jails, and does this backed directly by zfs
snapshots. So we can maintain a single image of our software stack, and
then blend

[Jails]: https://docs.freebsd.org/44doc/papers/jail/jail.html
[architecture]: https://www.freebsd.org/doc/en_US.ISO8859-1/books/arch-handbook/jail.html
[jail docs]: https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/jails.html
[ZFS]: https://www.ixsystems.com/whats-new/why-we-use-zfs/
[intro]: https://www.ixsystems.com/whats-new/intro-to-zfs-what-is-zfs/
[bitrot]: http://lmgtfy.com/?q=bit+rot
[VMWare Fusion]: https://vmware.com/products/fusion/
[OpenZFS on OSX]: https://openzfsonosx.org/
