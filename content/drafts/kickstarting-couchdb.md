---
title: "Kickstarting CouchDB"
date: 2012-05-21
draft: true
comments: true
categories: 
    - couchdb
    - kickstart
---
Kickstart is a series of posts about getting up and running with CouchDB,
for people who are just starting out with web programming and CouchDB.

If you're struggling to understand something along the way, get in touch.

Pre-requisites:

- you've read the Definitive Guide
- you're running a recent version or windows, linux, or mac
- you have curl and erica installed on your system
- you have access to a CouchDB 1.2.0 instance (iriscouch or local install)

First up, let's create a new empty couchapp using erica:

{% codeblock create a new couchapp lang:bash %}
dave@akai r % erica create-app appid=mustache
==> r (create-app)
Writing mustache/_id
Writing mustache/language
Writing mustache/.couchapprc
dave@akai r % cd mustache
dave@akai mustache % tree -Fa
.
├── .couchapprc
├── _attachments/
├── _id
└── language

1 directory, 3 files
dave@akai mustache %
{% endcodeblock %}

Let's take a closer look:

- ```.couchapprc``` is a file that defines where, by default, this couch app
