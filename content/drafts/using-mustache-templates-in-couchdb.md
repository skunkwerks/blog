---
title: "Using Mustache templates in CouchDB"
date: 2012-05-21
draft: true
comments: true
categories:
    - couchdb
    - templating
    - kickstart
---

If you are starting out with CouchDB it can be a tad daunting to get
started. This is a series of [kickstart] posts
