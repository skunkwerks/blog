---
categories: []
date: 2015-08-06T23:01:57+02:00
keywords: []
title: All Dockered Out
draft: true
---

Unless you've been under a rock for the last 2 years, you'll be aware that
containerising everything is the new hotness, except when ... it's not new.
Containers, in various forms, have been around since 2000, and arguably
earlier in some form in mainframe systems.

I have been playing with docker the last fortnight, and while the container
concept is great, the docker implementation is, by their own admissions, not
considered stable enough for production use.

That aside, the three main use cases put forward are to:

1. Speed developer time by re-using build & dev artefacts quickly and
   reliably.
2. Simplify testing & production deployment by using exactly the same
   artefacts as in development.
3. Enable more composable architectures by providing 

fsafdsagfhdjsahfdsajfgdhsajkfghdsajk
fdsahfjdskagfhdjsak
