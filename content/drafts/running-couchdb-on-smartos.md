---
title: "Running CouchDB on SmartOS"
date: 2012-10-25
draft: true
comments: true
categories:
    - couchdb
    - smartos
---
A common thread has appeared in my day-to-day work recently -- using
[DTRace] to identify performance problems, invalid filesystem accesses,
and  using [zfs] on my mac, mainly for managing snapshots
of various virtual machines for quick rollback to a known good state,
and to ensure that I can always rely on the integrity of my filesystem.

Both of these were developed at Sun Microsystems and were open
sourced under their governance. Oracle of course have stopped that, but
once the gate has been opened it's hard to go back.

There's a core [Illumos] project that is the original fork from Sun, and
from that fork are at least two interesting projects, built largely around
the same components:

- a KVM hypervisor
- built with ZFS
- DTrace integrated
- using Crossbow architecture for networking
- Zones for containment/security

More on those later.

- Joyent's [SmartOS] which is what I'm playing with today, oriented towards datacentre virtualisation
- [OmniOS] with the same core technologies, but in the spirit of a long-running server or workstation

Enough! Let's get started!

## Installing SmartOS

This is pretty straightforwards, make a VM (Solaris x64 compatible), burn
a CD, make a USB stick, and follow the instructions. Note this works best
when you can dedicate the whole disk to SmartOS.

I'm going to launch this in Joyent's cloud, using a `base64` image.

{% codeblock Installing the Package Manager lang:bash %}
# get the bootstrap package manager - URL will change over time
cd /tmp && curl -#LO http://pkgsrc.joyent.com/sdc/2012Q2/x86_64/bootstrap.tar.gz
cd / && gzcat /tmp/bootstrap.tar.gz | tar -xf -
# rebuild the package db
pkg_admin rebuild
pkgin -y up
# fix annoying stuff
touch /var/db/locate.database
chmod 644 /var/db/locate.database
updatedb 2>&1 > /dev/null
{% endcodeblock %}

{% codeblock Install Tools and Dependencies lang:bash %}
# upgrade all old packages
sm-rebuild-pkgsrc
pkgin -y full-upgrade
# tools and compilers
pkgin -y in scmgit-base gcc47 erlang zsh
pkgin -y rm nodejs
pkgin -y in nodejs-0.8.11
## get our own copy of pkgsrc & its submodules
git clone --recursive git://github.com/joyent/pkgsrc.git /opt/pkgsrc
git clone --recursive -b pkgsrc_2012Q1 git://github.com/mamash/pk.git /opt/pk
# update path
sed -i'' '\,^PATH,s,$,:/opt/pk/bin,' ~/.profile && source ~/.profile
pk info
{% endcodeblock %}
{% codeblock install some couchdb dependencies lang:bash %}
# dependencies
pkgin -y icu
{% endcodeblock %}
[zfs]: http://code.google.com/p/maczfs/
[dtrace]: http://www.dtrace.org/
[illumos]: https://www.illumos.org/
[smartos]: http://smartos.org/
[omnios]: http://omnios.omniti.com/

http://wiki.joyent.com/wiki/display/jpc2/The+Joyent+Linux-to-SmartOS+Cheat+Sheet
