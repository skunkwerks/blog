---
title: "Working with Rebar"
date: 2012-10-26
comments: true
draft: true
categories:
    - rebar
    - erlang
---

[rebar] is a build tool from [basho], written in Erlang, that facilitates building and distributing Erlang/OTP applications. 

We'll be using it to 

# A first app

    mkdir ~/playground/erlang/helloapp
    cd ~/playground/erlang/helloapp
    rebar list-templates
    rebar create template=simpleapp appid=helloapp

# compile

    rebar compile    

[rebar]: https://github.com/basho/rebar
[basho]: https://www.basho.com/
