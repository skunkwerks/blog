---
date: 2014-07-03T10:49:22Z
draft: false
title: about
menu: main
---

## Business Activities

- Founder & CTO of [SkunkWerks GmbH], bringing distributed systems
  innovation to the edges of the Internet
- deep expertise for companies relying on Apache CouchDB
- design and operations for reliable robust distributed systems

## Community Involvement

- Lead Developer of the [Swirl Project], implementing multi-peer
  live streaming in Erlang/OTP
- Ports Committer at the [FreeBSD project]
- Member of the [Apache Software Foundation]
- Committer & PMC member for the [Apache CouchDB] project, the database
    that replicates
- Organiser of [Vienna BEAMers] meetup, dedicated to [Elixir], [Erlang], and
    Distributed Systems in general

## Public Keys

- [openssh] public keys
- general [email openpgp keys] and dedicated [code signing key]

## Contacts & Social Media

- Twitter: [@dch\_\_]
- LinkedIn: [Dave Cottlehuber]
- Email: `dch@` any of the domains mentioned above
- Code and content: [dch] or [skunkwerks] at GitHub
- Call: `sip:dch@skunkwerks.at` or `skype` by arrangement
- IRC: find `dch` in `irc://irc.freenode.net/#couchdb` or
    `irc://chat.efnet.org/#bsdports`
- phone: `+43 67 67 22 44 78`
- webRTC: https://appear.in/skunkwerks
- postal:

    ```
    SkunkWerks GmbH
    Naufahrtweg 153
    1220 Wien
    Austria
    ```

## Useful Maps

Credits to [OpenStreetMap]

- [Biberhaufen](../maps/wien_biberhaufen.png)
- [Hauptbahnhof](../maps/wien_hauptbahnhof.png)
- [Naufahrtweg](../maps/wien_naufahrtweg.png)

[OpenStreetMap]: http://www.openstreetmap.org/
[SkunkWerks GmbH]: http://www.skunkwerks.at/
[Swirl Project]: http://www.swirl-project.org/
[Apache Software Foundation]: https://apache.org/
[FreeBSD project]: https://www.freebsd.org/
[@dch\_\_]: https://twitter.com/dch__
[Dave Cottlehuber]: https://linkedin.com/in/davecottlehuber
[dch]: https://github.com/dch
[skunkwerks]: https://github.com/skunkwerks
[Apache CouchDB]: https://couchdb.apache.org/
[Vienna BEAMers]: http://www.meetup.com/Vienna-BEAMers/
[openssh]: ../authorized_keys
[email openpgp keys]: ../KEYS.asc
[Elixir]: http://elixir-lang.org/
[Erlang]: http://erlang.org/
[code signing key]: ../E0AF0A194D55C84E4A19A801CDB0C0F904F4EE9B.asc
